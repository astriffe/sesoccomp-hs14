var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase('http://localhost:7474');
var GithubApi = require('node-github');

var github = new GithubApi({
	version: "3.0.0"
});

github.authenticate({
		type: 'basic',
		username: 'lexruee', // your username
		password: '------' // your password
});

function getFollowers(username,whenDone){
	github.user.getFollowers({
		user: username
	}, function(err,users){
		var store = []
		users.forEach(function(user){
			store.push( [user.login,'follows',username] );
		});
		whenDone(store);
	});	
}

getFollowers('wanze',function(tuples){
	tuples.forEach(function(tuple){
		var user1 = db.createNode({user: tuple[0] });
		user1.save(function(err,res){
			if(!err){
				var user2 = db.createNode({user: tuple[2] });
				user2.save(function(err,res){
					if(!err){
						relationship = user1.createRelationshipFrom(user2,'FOLLOWS',{},
							function(err, relationship){});
						
					}
				});
			}
		});
	});
});

