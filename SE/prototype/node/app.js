var GithubApi = require('node-github');

var github = new GithubApi({
	version: "3.0.0"
});

github.authenticate({
		type: 'basic',
		username: 'lexruee', // your username
		password: '------' // your password
});

function getFollowers(username,whenDone){
	github.user.getFollowers({
		user: username
	}, function(err,users){
		var store = []
		users.forEach(function(user){
			store.push( [user.login,'follows',username] );
		});
		whenDone(store,users);
	});	
}

function getRepos(username,whenDone){
	github.repos.getFromUser({
		user: username
	},function(err,repos){
		var store = [];
		repos.forEach(function(repo){
			store.push({ 
				name: repo['name'], 
				lang: repo['language'], 
				forks: repo['forks'],
				watchers: repo['watchers']
			})
		});
		whenDone(store);
	});
}

function getCommits(username,reponame,whenDone){
		github.repos.getCommits({
		user: username,
		repo: reponame
	},function(err,commits){
		var store = [];
		commits.forEach(function(commit){
			entry = {
				username: commit.author.login,
				author: commit.commit.author.name,
				message: commit.commit.message,
				date: commit.commit.author.date,
				email: commit.commit.author.email
			};
			
			if(entry.username == username)
				store.push(entry);
			
		});
		whenDone(store);
	});
}

whenDone = function(tuples,users){
	tuples.forEach(function(tuple){
		console.log(tuple);
	});
};

getFollowers('wanze',whenDone);

getRepos('wanze',whenDone);

getCommits('wanze','CarTrading',whenDone);
