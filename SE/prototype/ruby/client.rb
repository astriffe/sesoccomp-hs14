require 'octokit'
require 'pp'
require 'json'

$client = Octokit::Client.new \
  :client_id     => "c35f97b23fcf97b8f881",
  :client_secret => "15741b97a65c3025ac0b4890cace58118644b5e4"


def get_follower_graph(username,level,tree,visited)
	# base case
	if level == 0
		return
	end

	# get current user
	user = $client.user username
	visited << username # add user to visited
	
	followers = $client.get user[:followers_url]

	tree[:name] = username
	tree[:children] = followers.map { |follower| {name: follower[:login], children: []} }

	# filter followers to avoid cycles.
	followers.each do |follower|
		sub_tree = tree[:children].select { |e|  e[:name] == follower[:login] }.first
		get_follower_graph(follower[:login],level-1,sub_tree,visited)
	end
end

tree = Hash.new
followers = Array.new

get_follower_graph 'lexruee', 3, tree, followers

File.open('./out.json','w') do |f|
	f.write(JSON.pretty_generate(tree))
end
