require './lib/config'
module GBuddy
  include Config

  class User
    include Config

    class << self

      def find(username)
        neo.execute_query("MATCH (node:User { login: '#{username}'}) RETURN head(node)")
      end


      def build_follower_graph(username)
        tree = Hash.new
        followers = Array.new
        get_follower_graph(username, 1, tree, followers)
        JSON.pretty_generate(tree)
      end


      private
      def get_follower_graph(username, level, tree, visited)
        # base case
        if level == 0
          return
        end

        # get current user
        user = octokit.user username
        visited << username # add user to visited

        followers = octokit.get user[:followers_url]

        tree[:name] = username
        tree[:children] = followers.map { |follower| {name: follower[:login], children: []} }

        # filter followers to avoid cycles.
        followers.each do |follower|
          sub_tree = tree[:children].select { |e| e[:name] == follower[:login] }.first
          get_follower_graph(follower[:login], level-1, sub_tree, visited)
        end

      end

    end

  end





end