
configure do
  set :public_folder, 'public'
end


get '/' do
  erb :index, :layout => :main
end


get '/users/:name' do
  content_type :json
  user = GithubBuddy::User.find(params[:name])
  json user
end


get '/users/:name/followers' do
  content_type :json
  followers = GithubBuddy::User.followers(params[:name])
  json followers
end


get '/users/:name/graph' do
  content_type :json
  graph = GithubBuddy::User.follower_graph(params[:name])
  json graph
end