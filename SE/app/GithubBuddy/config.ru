Bundler.require(:default,:development)

# require all lib files
$LOAD_PATH.unshift( File.join( File.dirname(__FILE__), 'lib' ) )
require 'pp'
require './app'
require './lib/github_buddy'

run Sinatra::Application