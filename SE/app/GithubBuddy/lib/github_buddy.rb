require 'github_buddy/settings'
module GithubBuddy
  include Settings


  Github = GithubBuddy::Github::Client

  module User

    @wanted_keys = [:login, :email, :blog, :location, :company, :name]


    def self.find(username)
      Github.user(username).to_hash
    end

    def self.followers(username)
      user = Github.user(username)
      followers = Github.get(user[:followers_url])
      followers = followers.map do |follower|
        follower[:login]
      end
    end

    def self.followings(username)
      user = Github.user(username)
      followings = Github.get(user[:following_url].href)
      followings = followings.map do |follower|
        follower[:login]
      end
    end


    def self.follower_graph(username)
      graph = Hash.new
      create_follower_graph(username, 2, graph, Array.new)
      graph
    end


    private
    def self.create_follower_graph(username, level, tree, visited)
      # base case
      if level == 0
        return
      end

      # get current user
      user = Github.user username
      visited << username # add user to visited

      followers = Github.get user[:followers_url]
      #followers =  followers.select { |f| not visited.include?(f[:login])}
      tree[:name] = username
      tree[:children] = followers.map { |follower| {name: follower[:login], children: []} }

      # filter followers to avoid cycles.
      followers.each do |follower|
        sub_tree = tree[:children].select { |e| e[:name] == follower[:login] }.first
        create_follower_graph(follower[:login], level-1, sub_tree, visited)
      end

    end

  end

end