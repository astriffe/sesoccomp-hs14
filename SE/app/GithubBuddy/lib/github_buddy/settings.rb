require 'octokit'
require 'neography'

module GithubBuddy
  module Settings
    def self.included(base)
      base.extend(Neo4j)
      base.extend(Github)
    end

    module Neo4j
      # these are the default values:
      Neography.configure do |config|
        config.protocol = "http://"
        config.server = "localhost"
        config.port = 7474
        config.directory = "" # prefix this path with '/'
        config.cypher_path = "/cypher"
        config.gremlin_path = "/ext/GremlinPlugin/graphdb/execute_script"
        config.log_file = "neography.log"
        config.log_enabled = false
        config.slow_log_threshold = 0 # time in ms for query logging
        config.max_threads = 20
        config.authentication = nil # 'basic' or 'digest'
        config.username = nil
        config.password = nil
        config.parser = MultiJsonParser
        config.http_send_timeout = 1200
        config.http_receive_timeout = 1200
        config.persistent = true
      end

      Client = Neography::Rest.new

      def neo
        Client
      end

    end

    module Github
      Client = Octokit::Client.new(
          :client_id => "c35f97b23fcf97b8f881",
          :client_secret => "15741b97a65c3025ac0b4890cace58118644b5e4")

      def octokit
        Client
      end

    end
  end
end