#Github Buddy

##Requirements

- node.js and npm
- bower
- rvm and ruby 2.1.2
- IDE: rubymine


Rubymine:
`
https://www.jetbrains.com/ruby/
`

Install node.js:
`
http://nodejs.org/download/
`

Install bower:
`
sudo npm install -g bower
`

Install rvm:
`
http://rvm.io/rvm/install
`
E.g:
`
\curl -sSL https://get.rvm.io | bash -s stable --ruby
`

Install ruby 2.1.2:
`
rvm install ruby-2.1.2
`



##Run the app using a terminal
Change to the app directory.

Set the ruby version to 2.1.2:
`
rvm use 2.1.2
`

Install all dependencies:
`
bower install
`

`
bundle install
`
Finally run the app:
`
bundle exec rackup app.rb config.ru
`

##Run the app using Rubymine
Click on the config.ru file in the project view and click on run.