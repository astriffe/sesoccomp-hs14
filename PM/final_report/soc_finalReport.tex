\newcommand{\lecture}{Seminar Social Computing} 
\newcommand{\serieNumber}{HS 2014} 
\newcommand{\authorsOfSerie}{R\"uedlinger, Striffeler*, Wanzenried}
\author{Alexander R\"uedlinger (a.rueedlinger@gmail.com)\\Alexander Striffeler* (a.striffeler@students.unibe.ch)\\Stefan Wanzenried (stefan.wanzenried@gmail.com)}

\documentclass[a4paper]{scrartcl} 
%\documentclass[a4paper]{article} 
\usepackage{../documentstyle}
\usepackage{float}
\usepackage{listings}
\usepackage{graphicx}

\headheight=30pt


%=================================
%	Warning Box definition
%=================================
%\usepackage{framed}
%\definecolor{peach}{rgb}{1.0, 0.9, 0.71}
%\definecolor{platinum}{rgb}{0.9, 0.89, 0.89}
%\renewenvironment{leftbar}[1][\hsize]%
%{%
%        \def\FrameCommand%
%        {%
%             \includegraphics[width=1.2cm]{fig/pitfall.png}%           
%             \fboxsep=\FrameSep\colorbox{platinum}%
%        }%
%        \MakeFramed{\hsize#1\advance\hsize-\width\FrameRestore}%
%}%
%{\endMakeFramed}


\begin{document} 
\input{../header} 

\title{GitHub Explorer}
\subtitle{Seminar Social Computing HS 2014\\Final Report}
\maketitle

\tableofcontents
\newpage

\section{Introduction}
GitHub\footnote{\url{http://www.github.com}} is a widely used platform to manage and visualize Git repositories. Moreover, the platform offers possibilities to interact with other users by following them or starring certain repositories. As you might guess, the follower-approach is quite twitterish.

However, having many followers and observing a lot of followees, a user might ask himself what role he plays in this huge network? How are his friends interconnected? Are there interesting projects maintained by his colleagues? Can you detect projects that bring together many good programmers and as such act as a kind of network hub?

\emph{GitHub Explorer} aims to answer these questions mentioned just above in an easy and amusing way. Section \ref{se:usecases} will show in detail how this objective is pursued.

\section{Usecases / Views}\label{se:usecases}
Our \emph{GitHub Explorer} application will consist of two principal views: 
\begin{itemize}
\item \textbf{Explore Zone:} This view allows the user to navigate through the GitHub network graph starting from a given node. In this use case, the user does not need to have any knowledge of the network or any goals on what to discover. Instead, he can simply explore what is around a certain node (while a node can be either a repository or a user). This view, however, is not just a good starting point for users to take a look at the network topology but also enables the application to grasp network data. Based on the nodes the users click on, the application will request data from GitHub. Hence, our application relies on the curiosity of the users to discover the environment of specific nodes.

\item \textbf{Query Zone:} This second view is highly dependant on what nodes the users have explored in the \textit{explore zone}. Based on the data in the database, the users will now be able to query the network graph according to their individual goals by executing pre-defined queries against the persistent database. These queries allow the user to explore the relationships between several nodes or to detect nodes with specific properties.
\end{itemize}


Figure \ref{fig:exploreView} shows a screenshot of the \emph{explore zone}. We can see that the central part shows the visited nodes, the currently active node and the path we followed in between. The top right buttons allow us to display or hide both repository nodes and user nodes. The right side pane provides a legend that helps us identifying the three symbols for nodes as well as the various colors that indicate different node relationships. The left side pane, however, gives us further details on the active, i.e., clicked, node: Profile information, followers, followees, repositories, etc.

\begin{figure}
\begin{centering}
\includegraphics[scale=0.2]{fig/ss_explore_view.png}
\caption{Screenshot of Explore zone, visualizing node interconnections along a node path the user followed.}
\label{fig:exploreView}
\end{centering}
\end{figure}


Figure \ref{fig:queryZone} shows a screenshot of our second view, the \emph{query zone}. As described above, this view enables the user to execute a pre-defined query set to the persistent data. Currently, we provide three pre-defined and customizable queries: Shortest path, common nodes and repositories. We can think of either expanding the pre-defined query set or introducing a user-driven query bilder as a future task.

\begin{figure}
\begin{centering}
\includegraphics[scale=0.35]{fig/ss_query_zone.png}
\caption{Screenshot of Query zone.}
\label{fig:queryZone}
\end{centering}
\end{figure}


One of our major challenges was to decide on which nodes to display (or rather which nodes to hide) in the \textit{explore zone}. We achieved the best results with the following characteristics:
\begin{itemize}
\item Show all outgoing links and the target nodes for the currently active node.
\item Show all nodes on the path that led to the currently active node.
\item Hide outgoing relations and the target nodes from such path nodes, except:
\item Show in- or outgoing links to nodes that share a relationship to the currently active node.
\item Load a limited set of neighbor nodes with each click: Each click on a node loads additional 20 neighboring nodes. Thus, the user can influence how much node relations to show.
\item Allow the user to hide or show repository and / or user nodes.
\end{itemize}
Above rules efficiently reduce the number of nodes that are displayed in the explore zone and significantly improve the overview. However, for very large repositories or active user nodes, we have to paginate outgoing links from the active node. This prevents that the view gets all messed up with nodes and relationships if a large repository or a very active user is being clicked.

\section{Data Model}
We persistently store all data the GitHub API\footnote{\url{https://developer.github.com/v3/}} provides us with. While we do not show all of this information in the visualizations at the moment, this preserves the possiblity to easily extend the application in future. 


Looking at nodes, we visualize both users and repositories. Those nodes can be related to each other by one of the following relationships:
\begin{itemize}
\item (User) -- [stars] $\rightarrow$ (Repository)
\item (User) -- [owns] $\rightarrow$ (Repository)
\item (User) -- [contributes] $\rightarrow$ (Repository)
\item (User) -- [follows] $\rightarrow$ (User)
\end{itemize}

In addition to the given API data, we introduce a node property that stores the date at which the element was fetched last. This allows us to invalidate node information after a certain amount of time and thus to keep our database up to date. Old nodes can either be re-fetched in order to get the latest data, invalidated or even completely be removed from the system. Currently, if a clicked node is older than five days, we update its properties by again fetching the node from GitHub. We can also use this property to delete seldom requested nodes from our persistent storage in the context of a database clean-up in order to free resources for more current nodes.

\section{Technology}
As we were building some prototype implementations, we realized that the main challenge of our project does not primarily consist of technical issues but rather in the choice of a meaningful yet clearly arranged representation of the node graph. Nevertheless, we will describe our different technological approaches in this section. Figure \ref{fig:layers} shows how the different layers are connected to each others. In Addition, Figure \ref{fig:sequence} presents a sample request and the way it will be processed by the different components.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.2]{fig/components.png}
\caption{Different layers used in \emph{GitHub Explorer}.}
\label{fig:layers}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.6]{fig/ssc_sequence_diagram.png}
\caption{Sequence Diagram for sample request.}
\label{fig:sequence}
\end{figure}

\subsection{Presentation Layer: D3}
To visualize our graph data, we use the framework D3.js\footnote{\url{http://www.d3js.org}}. This JavaScript library provides us with highly customizable graphs based on our GitHub network data. Upon user input, the presentation layer sends a GET request to the data controller, which will respond with an array of matching nodes and their relationships. The functionality of the data controller is described in section \ref{se:datacontrol}. The whole front end is fully AJAX driven.

\subsection{Data Controller}\label{se:datacontrol}
The presentation layer interacts solely with our control layer, which provides a RESTful API. We use the framework Sinatra\footnote{\url{http://www.sinatrarb.com}} to implement a light web service using the Ruby programming language. The main task of the control layer is to keep track on which data can be delivered from the persistent storage and which data has to be requested from the GitHub API on demand. Thus, the data controller actually is the point where all comes together. Figure \ref{fig:dataProcess} shows the handling of a request inside the data controller.

\begin{figure}[htbp]
\begin{centering}
\includegraphics[scale=0.5]{fig/dataControl_sequence.png}
\caption{Flow chart describing the decisions made by the data controller.}
\label{fig:dataProcess}
\end{centering}
\end{figure}


The Data Controller will perform data fetching in a two-step-approach:
\begin{enumerate}
\item \textbf{Clickfetch:} This type of fetching can also be described as a full fetch: Once a user clicks on a node and thus centers the node in its representation, we fetch all the information the GitHub API delivers.
\item \textbf{Neighborfetch:} As soon as a node appears at the end of an outgoing relationship of a clickfetched node, we fetch only basic data such as node name. Further, it is to be mentioned that outgoing links from a neighborfetched node are not captured. This mechanism helps us reducing both the loading time for requests and the storage footprint on the server.
\end{enumerate}

\subsection{Database}
We introduce another Ruby class which supports the data controller interacting with the database. Our so-called \verb+Neo4j_connector+ provides methods tailored to the usecases of our application. These methods then execute the appropriate CYPHER-Queries against the RESTful API of our Neo4j\footnote{\url{http://www.neo4j.org}} server instance.

We are fully aware that there would have existed multiple ready-to-use GEMs for the interaction with the Neo4j database. However, using our database connector class, we can achieve a maximum of encapsulation and transparency by having full control on the queries that are sent to the database. Further, we can completely abstract the data controller from the database interaction. If we decided to use another database system in future, the only consequence would be to adapt the database connector accordingly.

\subsection{Version Control}
All our project resources are version controlled using Git. The repository is hosted on Bitbucket and publicly accessible: \url{https://bitbucket.org/astriffe/sesoccomp-hs14}. Once the project is mature, we will of course move the repository to GitHub in order to fix this severe inconsistency.

Bitbucket does not only provide us with the Git repository but also delivers an issue tracking system which is well suited to interact within the team. 

\subsection{Deployment}
For testing our application in production, we have set up a dedicated server on Digital Ocean\footnote{\url{http://www.digitalocean.com}}. We have then added the specific directory of this server as a remote repository to our development Git configuration. Thus, deployment can be done by simply performing a git push:
\begin{lstlisting}
  $ git push deployment master 
\end{lstlisting}

This allows for both easily being able to deploy a new version to the production server and having the possibility of quickly reverting the deployment of a new version if something went wrong as the deployment server also maintains the full code history.


\subsection{Tests}
From early on, we have written test cases for our application. All these tests can be found in the \verb+test+ subdirectory of our project root folder. These tests do not only help us avoiding unrecognized side effects when introducing changes to the system, they also document our code.

\subsection{Technology: Conclusion}
As one can observe above, the communication between the different layers is fully data-driven as all of our thee layers handle HTTP request and JSON data. Also, by limiting the interaction of the components to their RESTful services, we achieve a high level of decoupling.

Our application contains two thresholds, which have to be adapted to the application usage: The age at which nodes get updated when they are requested and the age at which nodes get deleted from the system. While the former only influences the number of requests towards the GitHub API, the latter should be chosen very carefully since it has a great impact on the network information we can retrieve from the system.


\section{User Participation}
As every social application, our applications value highly depends on the user activity as we collect the data which is requested by the users in the explore zone.

\subsection{Incentive Techniques}
Our main incentive to the users is the value of the application itself. This means that the user can directly benefit from his own action in the explore zone, since: If a user wants to find nodes around a certain user (for instance) and this data is not present yet, he can add this data in the explore zone and will directly be able to benefit from his own activity. Further, our background jobs further leverage the value of the user activities.

Besides the fact that the user generates value for himself, we think that even exploring the network by hopping from node to node can be fun in first place and is a great pastime in boring moments such as meetings, train rides or conferences.

\subsection{User Feedback}
We have added a simple form where the user can help us in further improving the application by telling us what is missing. Of course, these suggestions then have to be rated after their importance and implemented one by one but we think that it is crucial to minimize the barriers for users to get in touch with the developers.

\section{Future Issues}
There are some features that can be considered being added once version one is up and running is up and running.

\subsection{Request control}
There is a limited amount of requests per time period we a re allowed to perform. To not infringe this policy, we could keep track of the number of still available requests in the current time period and inform the user if no more requests are available. Such a scenario paralyses the \textit{explore zone} completely. For the moment however, we are optimistic that this mechanism is not required since we expect the user base to be rather small.

\subsection{Custom Query Builder}
In the \emph{Query zone}, we currently provide a set of pre-defined interesting queries to search the graph after. En improvement of this view would be to allow the user to combine his own query elements for instance by dragging and dropping syntax elements and thus to create individual queries. However, first this requires the user to have at least some knowledge on the cypher language and second, the more possibilities a user has to modify queries, the larger are security concerns regarding malicious users.

\subsection{Enhance Data Set}
We can think of performin a cron job, which would improve the data set in a regular manner. In this context, we could take the set of all neighborfetched nodes and fully fetch (a.k.a clickfetch) them. As the batch job could use its own application key for the GitHub API, this would not influence the number of requests available to the user-driven part of the application. 
However, we do not recommend to remove old nodes, i.e., nodes that were not clicked for a long period of time, from the database as this would require to also all in- and outgoing links from or to these nodes. This again would reduce the interconnectivity of the nodes, might brake paths between certain nodes and thus have a negative impact on the overall network view. Hence, this routine should only be executed if storage and/or performance restrictions require.


\section{Conclusion}
Acting as a proof-of-concept, our application has shown that visualizing node relationships in the GitHub network can reveil interesting insights how users and / or repositories are connected. Even when dealing with nodes having many relationships, we have seen that our approaches to narrow the result set allow us to keep the overview high and thus to enable the user to retrieve network information. By adding further queries in future, we could leverage the value of the retrieved data by for instance searching nodes based on a similarity metric.

However, we have also seen that the application stores large amounts of data. Together with high hardware requirements of the components themselves (Neo4j recommends 16-32 GB RAM), our application as a whole demands for high performant hardware.

\end{document}