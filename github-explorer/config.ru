require 'rubygems'
require 'bundler'
require 'pp' # pretty printing
require 'json'
require './lib/neo4j_connector'
require './lib/neo4j_result'
require './lib/model'
require './lib/neo4j_proxy'
require './lib/github_proxy'
require './lib/data_controller'


Bundler.require(:default,:development)

require './app'
run Sinatra::Application
