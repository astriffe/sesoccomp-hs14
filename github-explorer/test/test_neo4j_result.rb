require 'test_helper'
require 'neo4j_connector'
require 'model'
require 'neo4j_result'
require 'neo4j_proxy'
require 'github_proxy'


# example output from the neo4 restful api
$json = '{
  "results": [
    {
      "columns": [
        "n1",
        "r",
        "n2"
      ],
      "data": [
        {
          "row": [
            {
              "name": "Stefan Wanzenried",
              "location": "Switzerland",
              "following": 8,
              "login": "wanze",
              "gists_url": "https://api.github.com/users/wanze/gists{/gist_id}",
              "organizations_url": "https://api.github.com/users/wanze/orgs",
              "public_gists": 0,
              "gravatar_id": "",
              "hireable": false,
              "url": "https://api.github.com/users/wanze",
              "repos_url": "https://api.github.com/users/wanze/repos",
              "public_repos": 13,
              "received_events_url": "https://api.github.com/users/wanze/received_events",
              "id": 2118742,
              "following_url": "https://api.github.com/users/wanze/following{/other_user}",
              "site_admin": false,
              "subscriptions_url": "https://api.github.com/users/wanze/subscriptions",
              "created_at": "2012-08-08 19:09:14 UTC",
              "starred_url": "https://api.github.com/users/wanze/starred{/owner}{/repo}",
              "html_url": "https://github.com/wanze",
              "type": "User",
              "updated_at": "2014-11-18 23:06:46 UTC",
              "events_url": "https://api.github.com/users/wanze/events{/privacy}",
              "avatar_url": "https://avatars.githubusercontent.com/u/2118742?v=3",
              "followers_url": "https://api.github.com/users/wanze/followers"
            },
            {
            },
            {
              "name": "2014_03_Cassandra_Lab",
              "owner": "#<Sawyer::Resource:0x000000015e7b20>",
              "url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab",
              "id": 18307595,
              "created_at": "2014-03-31 20:48:28 UTC",
              "html_url": "https://github.com/wanze/2014_03_Cassandra_Lab",
              "updated_at": "2014-04-18 20:04:25 UTC",
              "events_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/events",
              "tags_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/tags",
              "statuses_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/statuses/{sha}",
              "has_downloads": true,
              "blobs_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/git/blobs{/sha}",
              "git_refs_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/git/refs{/sha}",
              "issue_events_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/issues/events{/number}",
              "has_issues": true,
              "watchers_count": 0,
              "forks": 0,
              "private": false,
              "size": 144,
              "open_issues_count": 0,
              "open_issues": 0,
              "subscribers_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/subscribers",
              "stargazers_count": 0,
              "full_name": "wanze/2014_03_Cassandra_Lab",
              "releases_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/releases{/id}",
              "description": "Python scripts for Cassandra lab in the Advanced Database course",
              "trees_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/git/trees{/sha}",
              "branches_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/branches{/branch}",
              "pushed_at": "2014-03-31 20:50:41 UTC",
              "git_url": "git://github.com/wanze/2014_03_Cassandra_Lab.git",
              "collaborators_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/collaborators{/collaborator}",
              "subscription_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/subscription",
              "languages_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/languages",
              "has_wiki": true,
              "commits_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/commits{/sha}",
              "contents_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/contents/{+path}",
              "fork": false,
              "git_tags_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/git/tags{/sha}",
              "downloads_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/downloads",
              "svn_url": "https://github.com/wanze/2014_03_Cassandra_Lab",
              "milestones_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/milestones{/number}",
              "language": "Python",
              "compare_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/compare/{base}...{head}",
              "notifications_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/notifications{?since,all,participating}",
              "comments_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/comments{/number}",
              "pulls_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/pulls{/number}",
              "teams_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/teams",
              "forks_count": 0,
              "merges_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/merges",
              "keys_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/keys{/key_id}",
              "contributors_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/contributors",
              "forks_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/forks",
              "clone_url": "https://github.com/wanze/2014_03_Cassandra_Lab.git",
              "hooks_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/hooks",
              "ssh_url": "git@github.com:wanze/2014_03_Cassandra_Lab.git",
              "archive_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/{archive_format}{/ref}",
              "default_branch": "master",
              "issues_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/issues{/number}",
              "assignees_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/assignees{/user}",
              "watchers": 0,
              "issue_comment_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/issues/comments/{number}",
              "labels_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/labels{/name}",
              "has_pages": false,
              "git_commits_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/git/commits{/sha}",
              "stargazers_url": "https://api.github.com/repos/wanze/2014_03_Cassandra_Lab/stargazers"
            }
          ]
        }
      ]
    }
  ],
  "errors": [

  ]
}'

class TestNeo4jResult < MiniTest::Test

  def setup
    @neo4j_proxy = Neo4jProxy.new
  end

  def test_create_result

    result = Neo4jResult.from_json $json

    assert result.size == 1
    user_hash = result[0][0] # select first column
    relationship_hash = result[0][1] # select second column
    repo_hash = result[0][2] # select third column

    assert_equal 'wanze', user_hash['login']
    assert_equal '2014_03_Cassandra_Lab', repo_hash['name']

  end

  def test_query
    users = @neo4j_proxy.query 'MATCH (n:User) RETURN n;'
    assert users.is_a? Neo4jResult::Table
    assert users.size >=1
  end

  def test_each_on_table
    table = @neo4j_proxy.query 'MATCH (n:User) RETURN n;'
    assert table.is_a? Neo4jResult::Table
    table.each do |row|
      assert row.is_a? Neo4jResult::Row
    end
  end

  def test_each_on_rows
    table = @neo4j_proxy.query 'MATCH (n:User) RETURN n;'
    assert table.is_a? Neo4jResult::Table
    table.each do |row|
      row.each do |item|
        assert item.nil? == false
      end
    end
  end

  def test_map_on_table
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.is_a? Neo4jResult::Table
    users = table.map {|row| row[0] }
  end

  def test_select_on_table
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.is_a? Neo4jResult::Table
    users = table[0].select {|item| item['type'] == 'User' }
  end

  def test_detect_on_table
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.is_a? Neo4jResult::Table

    users = table.map {|row| row.detect {|item| item['type']=='User' } }
    repos = table.map {|row| row.detect {|item| item['type']=='Repository' } }
    #pp users
    #pp repos
  end

  def test_selecting_element_from_table
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.is_a? Neo4jResult::Table
    assert table[0,0].nil? == false
    #pp table[0,0]
  end

  def test_selecting_column_name_on_table
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.is_a? Neo4jResult::Table

    users = table['n'] # select all rows by considering only the column name 'n'
    assert users.nil? == false

    user = table[0,'n'] # select first row by considering only the column name 'n'
    assert user.nil? == false
  end

  def test_star_operation_on_table_using_numeric_index
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.is_a? Neo4jResult::Table

    users = table[:*,0]
    assert users.nil? == false
  end

  def test_star_operation_on_table_using_column_name
    table = @neo4j_proxy.query 'MATCH (user:User)-[relationship:Owns]->(repository:Repository) RETURN user,relationship,repository;'
    assert table.is_a? Neo4jResult::Table

    users = table[:*,'user']
    assert users.nil? == false
    users.each do |user|
      assert user['type'] == 'User'
    end

    repos = table[:*,'repository']
    repos.each do |repo|
      assert repo['type'] == 'Repository'
    end
    repos = table['repository']
    repos.each do |repo|
      assert repo['type'] == 'Repository'
    end
    #pp repos
  end

  def test_column_pick_on_table
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.is_a? Neo4jResult::Table

    users = table.pick_column 'n'
    assert users.nil? == false

  end

  def test_if_table_is_empty
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    assert table.empty? == false
  end

  def test_first_on_table
    table = @neo4j_proxy.query 'MATCH (n:User)-[r:Owns]->(u) RETURN n,r,u;'
    first_row = table.first
    assert first_row.nil? == false
    #pp first_row
  end

  def test_should_fetch_user_from_neo4j
    user = GithubProxy.user 'wanze'
    user = @neo4j_proxy.fetch_user_by_id user.id
    assert user.nil? == false
    #pp user
    #pp user.repositories.map {|r| r.name}
    #pp user.followers.map {|f| f.login}
  end

  def test_should_fetch_repo_from_neo4j
    repo = GithubProxy.repository 'ruby/ruby'
    repo= @neo4j_proxy.fetch_repository_by_id repo.id
    assert repo.nil? == false
    #pp repo.contributors.map {|u| u.login}
  end

  def test_relationship_should_exist
    user = GithubProxy.fetch_user 'wanze'
    other_repo = GithubProxy.fetch_repository 'ruby/ruby'
    repo =  user.repositories.first
    assert @neo4j_proxy.relationship_exists? first_node: user, second_node: repo, label: "Owns"
  end

  def test_relationship_should_not_exist
    user = GithubProxy.fetch_user 'wanze'
    other_repo = GithubProxy.fetch_repository 'ruby/ruby'
    assert !(@neo4j_proxy.relationship_exists? first_node: user, second_node: other_repo, label: "Owns")
  end

  def test_should_exist_user
    user = @neo4j_proxy.user_exists? login: 'wanze'
    assert user
    assert user != false
  end
end