require 'test_helper'
require 'model'
require 'github_proxy'

class TestGithubClient < MiniTest::Test

  def test_should_get_a_github_client
    client = GithubProxy::CLIENT
    assert client != nil
  end


  def test_should_get_user
    user = GithubProxy.user 'lexruee'
    assert_equal 'lexruee', user[:login]
  end


  def test_should_get_repo
    repo = GithubProxy.repository 'ruby/ruby'
    assert_equal 'ruby/ruby', repo[:full_name]
  end

  def test_should_get_garbage_user
    begin
    user = GithubProxy.user 'sdjkfjksdjkfjksdnfjksdnf'
    rescue
      pp 'rescue'
    end

  end

  def test_should_get_garbage_repo
    begin
      repo = GithubProxy.repository 'sdjkfjksdjkfjksdnfjksdnf/sdfasdfs'
    rescue
      pp 'rescue'
    end
  end

  def test_should_get_following
    user = GithubProxy.user 'wanze'
    following = GithubProxy.get user[:following_url].gsub(/{\/other_user}/,'')
    pp following.map {|followed| followed.login}
  end

  def test_should_get_wanze
    user = GithubProxy.fetch_user 'wanze'
    puts user.to_json
  end

end