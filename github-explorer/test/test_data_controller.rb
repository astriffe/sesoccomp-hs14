require 'test_helper'

# require ordering does matter!
require 'neo4j_connector'
require 'neo4j_result'
require 'model'
require 'neo4j_proxy'
require 'github_proxy'
require 'data_controller'

class TestDataController < MiniTest::Test

  def setup
    @data_controller = DataController.new
  end

  def test_should_create_data_controller
    # pass dependencies
    data_controller_adapter_params = {
        neo4j_proxy: Neo4jConnector.new,
        github_proxy: GithubProxy
    }

    data_controller = DataController.new(data_controller_adapter_params)
    assert data_controller!= nil # dummy test
    assert data_controller.github_proxy != nil
    assert data_controller.neo4j_proxy != nil
  end


  def test_should_create_data_controller_using_default_values
    data_controller = DataController.new
    assert data_controller!= nil # dummy test
    assert data_controller.github_proxy != nil
    assert data_controller.neo4j_proxy != nil
  end


  def test_should_fetch_user
    user = @data_controller.fetch_user login: 'wanze'
    assert_equal 'wanze', user.login
  end

  def test_should_fetch_repository
    repository = @data_controller.fetch_repository full_name: 'ruby/ruby'
    assert_equal 'ruby', repository.name
  end

end