require 'test_helper'
require 'model'
require 'github_proxy'
require 'neo4j_connector'

class TestGithubProxy < MiniTest::Test

  @@verbose = true
  @@user = GithubProxy.fetch_user 'lexruee'


  def test_should_fetch_user
    user = GithubProxy.fetch_user 'lexruee'
    assert user.nil? == false
    assert_equal 'lexruee', user.login
  end


  def test_should_get_following
    assert @@user.following.is_a? Array
    assert !@@user.following.empty?
    @@user.following.each do |follower|
      assert follower.is_a? Model::UserView
      assert follower.fetch_time < DateTime.now
    end
  end


  def test_should_get_repos
    assert @@user.repositories.is_a? Array
    assert !@@user.repositories.empty?
    @@user.repositories.each do |repo|
      assert repo.is_a? Model::RepositoryView
      assert repo.fetch_time < DateTime.now
    end
  end


  def test_should_get_starred_repos
    assert @@user.starred.is_a? Array
    assert !@@user.starred.empty?
    @@user.starred.each do |repo|
      assert repo.is_a? Model::RepositoryView
      assert repo.fetch_time < DateTime.now
    end
  end


  def test_should_fetch_repository
    repo = GithubProxy.fetch_repository 'ruby/ruby'
    assert repo.is_a? Model::RepositoryView
    assert repo.fetch_time < DateTime.now
    repo.contributors.each do |user|
      assert user.is_a? Model::UserView
    end
  end


  def test_should_print_some_user_properties
    if @@verbose
      pp 'repos'
      pp @@user.repositories.map {|r| r.name}

      pp 'starred'
      pp @@user.starred.map {|r| r.name}

      pp 'following'
      pp @@user.following.map {|f| f.login}
    end
  end

end