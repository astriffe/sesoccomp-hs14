require 'test_helper'
require 'neo4j_connector' # this corresponds to ./lib/neo4j_connector
require 'model'
require 'github_proxy'
require 'neo4j_result'

# you can run this test with the rake command.
# cd to the root directory and run: rake test TEST=test/test_neo4j_connector.rb

class TestNeo4jConnector < MiniTest::Test

  # for more details see: http://ruby-doc.org/stdlib-1.9.2/libdoc/minitest/unit/rdoc/MiniTest/Unit/TestCase.html
  # and http://ruby-doc.org/stdlib-1.9.2/libdoc/minitest/unit/rdoc/MiniTest/Assertions.html

  @@cleared = false

  def setup
    @connector = Neo4jConnector.new()
    if @@cleared ==  false
      #@connector.clear_database
      @@cleared = true
    end
  end


  def test_create_neo4j_connector
    connector = Neo4jConnector.new()
    assert connector != nil # dumnmy test
  end


  # def test_create_nodes
  #   @connector.add_repository_node({'owner'=>'octocat', 'loc'=>543})
  #   @connector.add_user_node({'name'=> 'octodog', 'skills'=> 'roooar'})
  #   @connector.create_follows_relationship(21, 20, {})
  #
  #   nodes = JSON.parse(@connector.get_all_nodes())
  #   assert nodes != nil # dumnmy test
  #   assert nodes.is_a? Hash # dumnmy test
  #   puts nodes
  #
  # end

  def test_should_perform_query
    json = @connector.query "MATCH (n:User) RETURN n;"
    hash = JSON.parse json
    #pp hash
  end


  def test_should_find_shortest_path
    json = @connector.find_shortest_path('lexruee','astriffe')
    table = Neo4jResult.from_json json
    nodes = table[0,0]
    nodes = nodes.map {|a_hash| Model.from_neo4j a_hash}
    node_labels = table[0,1]
    rel_labels = table[0,2]
    shortest_path = Model::ShortestPathView.new nodes: nodes, node_lables: node_labels, rel_labels: rel_labels

    #pp shortest_path.to_view_hash
  end


  def test_should_not_find_shortest_path
    json = @connector.find_shortest_path('lexruee','matz')
    table = Neo4jResult.from_json json
    #assert table.empty?
  end


  def test_should_find_paths
    json = @connector.find_paths('lexruee','phiber')
    table = Neo4jResult.from_json json
    view = Model::MultiplePathView.new table
    pp view.to_view_hash
  end


  def test_should_query_repos
    json = @connector.find_repository_with [[ 'languages_count', '>', 3], 'and', [ 'languages_count', '<', 5]]
    table = Neo4jResult.from_json json
    #pp table[0,0]

    json = @connector.find_repository_with [[ 'language', '=', 'Ruby'], 'or', [ 'languages_count', '<', 5]]
    table = Neo4jResult.from_json json
    #pp table[0,0]


    json = @connector.find_repository_with [[ 'Ruby', 'in', 'language_names']]
    #pp json
    table = Neo4jResult.from_json json
   # pp table[0,0]

  end

end