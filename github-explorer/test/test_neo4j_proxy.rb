require 'test_helper'
require 'neo4j_connector'
require 'neo4j_result'
require 'model'
require 'neo4j_proxy'
require 'github_proxy'


class TestNeo4jProxy < MiniTest::Test

  @@cleared = false

  def setup
    @neo4j_proxy = Neo4jProxy.new
    if @@cleared == false
      @neo4j_proxy.clear_database
      @@cleared = true
    end

  end

  def test_should_create_neo4j_proxy
    proxy = Neo4jProxy.new
  end


  def test_should_add_user
    user = GithubProxy.fetch_user 'wanze'
    @neo4j_proxy.add_user(user)
    assert @neo4j_proxy.user_exists?(login: 'wanze')
  end


  def test_should_add_repository
    repo = GithubProxy.fetch_repository 'ruby/ruby'
    @neo4j_proxy.add_repository repo
    assert @neo4j_proxy.repository_exists?(full_name: 'ruby/ruby') #full name of a repository
  end


  def test_should_add_contributors_relationship
    repo = GithubProxy.fetch_repository 'ruby/ruby'
    @neo4j_proxy.add_repository repo
    repo.contributors.each do |contributor|
      @neo4j_proxy.add_user contributor
      @neo4j_proxy.create_contributes_relationship user: contributor, repository: repo
    end
  end


  def test_should_add_repositories_to_user
    user = GithubProxy.fetch_user 'wanze'

    @neo4j_proxy.add_user user

    user.repositories.each do |repo|
      @neo4j_proxy.add_repository repo
      assert @neo4j_proxy.repository_exists?(full_name: repo.full_name)
    end

  end


  def test_should_add_repo_owns_relationships
    user = GithubProxy.fetch_user 'wanze'

    @neo4j_proxy.add_user user

    user.repositories.each do |repository|
      @neo4j_proxy.add_repository repository
      assert @neo4j_proxy.repository_exists?(full_name: repository.full_name)
      @neo4j_proxy.create_owns_relationship user: user, repository: repository
    end

  end


  def test_should_add_repo_stars_relationships
    user = GithubProxy.fetch_user 'wanze'

    @neo4j_proxy.add_user user

    user.starred.each do |repository|
      @neo4j_proxy.add_repository repository
      assert @neo4j_proxy.repository_exists?(full_name: repository.full_name)
      @neo4j_proxy.create_stars_relationship user: user, repository: repository
    end

  end


  def test_should_add_followers_to_user
    user = GithubProxy.fetch_user 'wanze'

    @neo4j_proxy.add_user user

    user.followers.each do |follower|
      @neo4j_proxy.add_user follower
      @neo4j_proxy.create_follows_relationship followed: user, follower: follower
    end
  end


  def test_should_add_following_to_user
    user = GithubProxy.fetch_user 'wanze'

    @neo4j_proxy.add_user user

    user.following.each do |followed|
      @neo4j_proxy.add_user followed
      @neo4j_proxy.create_follows_relationship followed: followed, follower: user
    end
  end


  def test_should_add_and_delete_user
    assert @neo4j_proxy.user_exists?(login: 'rueedlinger') == false

    user = GithubProxy.fetch_user 'rueedlinger'

    assert @neo4j_proxy.add_user user
    assert @neo4j_proxy.user_exists?(login: 'rueedlinger')

    @neo4j_proxy.delete_user 'rueedlinger'
    assert !@neo4j_proxy.user_exists?(login: 'rueedlinger')
  end


  def test_should_perform_query
    result = @neo4j_proxy.query 'MATCH (n:User) RETURN n;'
    assert result.is_a? Neo4jResult::Table
  end


  def test_should_add_fetched_user
    fetched_user = GithubProxy.fetch_user 'mind4z'
    @neo4j_proxy.add_fetched_user(fetched_user)
  end


end