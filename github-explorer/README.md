#Github-Explorer

##Project Structure
```
lib/           contains ruby files like Neo4jConnector etc.
views/         contains views that are rendered by sinatra
app.rb         backend server - sinatra app
config.ru      config file (for future deployment on a heroku-like platform)
Gemfile        specifies project dependencies
Gemfile.lock   contains all resolved dependencies
```

##Deployment
For ssh and git access for the deployment server please send me your public ssh key:
```
cat ~/.ssh/id_rsa.pub
```

###Virtual Server 

Public ip:      104.236.114.114

Domain:		gh-explorer.lexruee.ch 

Neo4j-Browser:  104.236.114.114:7484


###Troubleshooting
Login to the server as root.

1) Check if apache2 is running

```
service apache2 status
service apache2 restart # if not running use this
```

2) Check if neo4j is running
```
service neo4j-service statis
service neo4j-service restart # if not running use this
```

3) Check if the sinatra app is running
```
ps -aux | grep unicorn
```
If not proceed as follows:
```
cd /var/www/github-explorer/github-explorer/
```

If some processes are still running, but the app does not work, kill the master process:
```
kill $(cat pids/unicorn.pid)
```

Start the app using the unicorn http server:
```
unicorn -c unicorn.rb -p 8080 -D
```

Check if unicorn is running:
```
ps -aux | grep unicorn
```


###Setting up the remote deployment server
Add the following remote to your local git repo:

Server 1
```
git remote add deployment git@104.236.26.23:deployment.git
```

Server 2:
```
git remote add deployment2 git@104.236.114.114:deployment.git
```


###Pushing to the remote deployment server
After setting up the remote deployment you can push to the deployment server as follows:
Server 1
```
git push deployment master
```

Server 2
```
git push deployment2 master
```


##Backend
###Setting everything up
Change to the github-explorer directory.
```
cd github-explorer
```

Install ruby 2.1.2 via rvm:

```
rvm install 2.1.2
```

Set the rvm version in the current directory:

```
rvm use 2.1.2
```

Install bundler:

```
gem install bundler
```

Run bundle to install all dependencies that are specified in the Gemfile:

```
bundle install
```


###Running the backend (app.rb)

Run the backend (app.b) via its config file using rackup:

```
rackup config.ru
```
Rack provides a minimal interface between webservers supporting Ruby and Ruby framework.
This might be useful if we want to deploy our app on heroku.

The backend should be accessible via:
```
http://localhost:9292
```

###Setting up Rubymine
1. Open Rubymine.
2. Go to File -> Open
3. Open github-explorer
4. Right click on the file config.ru (see left panel, project files)
5. Click on Run 'config.ru'

###Running tests
Change to the root directory (github-explorer).
```
rake test test/{name of the testfile}
```

Example - Running a single test:

```
rake test TEST=test/test_neo4j_connector.rb
```

Example - Running all tests:
```
rake test
```



###Writing tests
1) put all tests in the directory test

2) each test file must have a test suffix i.e.: test_blah.rb

3) running all tests: rake test

4) running a single test: rake test TEST=test/test_blah.rb






