configure do
  set :public_folder, 'public'
end

before do
  @data_controller = DataController.new # accessible through @data_controller
  @send_mail_to = ['a.rueedlinger@gmail.com', 'stefan.wanzenried@gmail.com', 'a.striffeler@students.unibe.ch']
end


get '/' do
  send_file 'public/index.html'
end


post '/contact' do

  res = begin
    json_payload = request.body.read
    a_hash = JSON.parse(json_payload)
    @send_mail_to.each do |email|
      Pony.mail({
                    :to => email,
                    :subject => 'Feedback! - %s' % DateTime.now.strftime('%Y-%m-%d, %H:%M'),
                    :body => a_hash['body'],
                    :via => :smtp,
                    :via_options => {
                        :address => 'smtp.gmail.com',
                        :port => '587',
                        :enable_starttls_auto => true,
                        :user_name => 'github.explorer@gmail.com',
                        :password => 'Chewbacca',
                        :authentication => :plain, # :plain, :login, :cram_md5, no auth by default
                        :domain => "localhost.localdomain" # the HELO domain provided by the client to the server

                    }
                })
    end

    {status: 'success'}
  rescue => e
    raise e
    {status: 'error'}
  end
  json res
end


get '/shortestpath' do
  q1, q2 = params[:q1], params[:q2]
  content_type :json
  begin
    a_hash = @data_controller.find_shortest_path(q1, q2)
    json a_hash
  rescue => e
    raise e
    a_hash = {status: 'error'}
    json a_hash
  end
end

get '/common_nodes' do
  q1, q2, limit, max_length = params[:q1], params[:q2], params[:limit].to_i, params[:length].to_i
  content_type :json
  begin
    limit = limit == 0 ? 45 : limit
    max_length = max_length == 0 ? 2 : max_length
    puts "length: %s" % max_length
    puts "limit: %s" % limit
    a_hash = @data_controller.find_paths(q1, q2, limit, max_length)
    json a_hash
  rescue => e
    raise e
    a_hash = {status: 'error'}
    json a_hash
  end
end


#
# ====Examples
# /users?id=38274892
# /users?q=wanze
#
get '/users' do
  content_type :json
  username, id = params[:q], params[:id].to_i
  begin
    a_hash = if username
               @data_controller.fetch_user login: username
             elsif id
               @data_controller.fetch_user id: id
             else
               raise 'Error'
             end

    json a_hash
  rescue => e
    raise e
    a_hash = {status: 'error'}
    json a_hash
  end
end


#
# ====Examples
# /repositories?id=38274892
# /repositories?q=wanze/Lexi
#
get '/repositories' do
  content_type :json
  begin
    if params[:q]
      repository_full_name = params[:q]
      a_hash = @data_controller.fetch_repository full_name: repository_full_name
      json a_hash
    else
      limit = params[:limit].to_i > 0 ? params[:limit].to_i : 25
      conditions = read_query_params(params)

      a_hash = if params[:random] == 'true'
                 @data_controller.find_random_repository(conditions)
               else
                 @data_controller.find_repository_with(conditions, limit)
               end
      json a_hash
    end
  rescue => e
    raise e
    a_hash = {status: 'error'}
    json a_hash
  end
end


def read_query_params(params)
  query_params = [:languages_count, :stargazers_count, :size, :forks_count]
  query_values = params.select { |k, v| query_params.include?(k.to_sym) }
  conditions = []
  query_values.each do |k, v|
    conditions << [k, '>', v.to_i] unless v.empty?
  end

  if params[:language]
    params[:language].each do |lang|
      lang = lang.gsub('"', '')
      conditions << [lang, 'in', 'language_names'] unless lang.empty?
    end
  end

  new_conditions = []
  conditions.each do |condition|
    new_conditions << condition
    new_conditions << 'and'
  end
  new_conditions.pop #remove last and
  pp new_conditions
  new_conditions
end