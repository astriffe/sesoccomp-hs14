/**
 * Returns an instance of the GitHub explorer
 *
 * @params $  Locally scoped JQuery object
 * @params d3 Locally scoped d3js object
 */
var github_explorer = (function ($, d3) {

    // Private variables
    var width;
    var height;
    var radius = 15;
    var paging = 15;
    //var clicked_first_node = false;
    var fetch_urls = {
        'user' : '/users?q=',
        'repository' : '/repositories?q='
    };
    var data = {
        "nodes": [],
        'links': []
    };
    var cache = {
        visited : d3.map(),
        nodes : d3.map()
    };
    var force;
    var svg;
    var node;
    var link;
    var selected_node = null;

    // Private functions

    /**
     * Initialize D3s
     */
    var initD3s = function () {
        force = d3.layout.force()
            //.friction(0.8)
            .friction(0.5)
            .linkDistance(150)
            .gravity(0.0)
            .charge(-500)
            .chargeDistance(500)
            .size([width, height])
            .nodes(data.nodes)
            .links(data.links)
            .on('tick', tick);
        svg = d3.select('#ge-canvas')
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox', '0 0 ' + width + ' ' + height);
        node = svg.selectAll('.node');
        link = svg.append('svg:g').selectAll('.link');
    };


    /**
     * Update graph simulation
     */
    function tick() {
        node.attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
        link.attr("x1", function (d) {
            return d.source.x;
        })
            .attr("y1", function (d) {
                return d.source.y;
            })
            .attr("x2", function (d) {
                return d.target.x;
            })
            .attr("y2", function (d) {
                return d.target.y;
            });
    }


    // Handler functions

    /**
     * On Node click
     * @param node
     */
    var onNodeClick = function(node) {
        if (d3.event.defaultPrevented) return; // ignore drag
        getNewData(node);
        // Send event
        $(document).trigger('onNodeClick');
    };


    /**
     * Compute some details and add them as properties to the nodes
     * @param node
     */
    var addDetailInfosToNode = function(node) {
      if (node.type == 'user') {
          addDetailInfosToUserNode(node);
      } else {
          addDetailInfosToRepositoryNode(node);
      }
    };


    /**
     * Add details to Repository node
     * @param repository_node
     */
    var addDetailInfosToRepositoryNode = function(repository_node) {

        var details = {
            programming_languages : [],
            stargazers : [],
            contributors : []
        };

        repository_node.details = repository_node.details || details;

        // Add programming languages
        var total_bytes = 0;
        var bytes_per_language = [];
        for (var i in repository_node.properties.language_names) {
            var language = repository_node.properties.language_names[i];
            bytes_per_language[language] = repository_node.properties.language_sizes[i];
            total_bytes += repository_node.properties.language_sizes[i];
        }

        for (var i in bytes_per_language) {
            var percentage = Math.floor((bytes_per_language[i] / total_bytes) * 100);
            if (percentage == 0) percentage = 1;
            repository_node.details.programming_languages.push({
                title : i,
                percentage : percentage
            });
        }

        // Add stargazers
        repository_node.links_incoming.forEach(function(link) {
            var node = cache.nodes.get(link.target);
            if (node) {
                if (link.type == 'stars') {
                    repository_node.details.stargazers.push(node);
                } else if (link.type == 'contributes') {
                    repository_node.details.contributors.push(node);
                }
            }
        });

    };


    /**
     * Add some more details to a User node used to display in the left sidebar
     * @param user_node
     */
    var addDetailInfosToUserNode = function(user_node) {

        var details = {
            repositories : [],
            starred_repositories : [],
            followees : []
        };

        user_node.details = user_node.details || details;

        //// Add repositories and followed users
        user_node.links_outgoing.forEach(function(link) {
            var node = cache.nodes.get(link.target);
            if (node) {
                if (node.type == 'repository') {
                    if (link.type == 'stars') {
                        user_node.details.starred_repositories.push(node);
                    } else {
                        user_node.details.repositories.push(node);
                    }
                } else if (node.type == 'user') {
                    user_node.details.followees.push(node);
                }
            }
        });

    };


    /**
     * Add all links between visited nodes to the graph
     * @param node
     */
    var addLinksOfVisitedNodesToGraph = function(node) {
        // Add all the links between the visited ones
        node.links_outgoing.forEach(function (link) {
            if (cache.visited.has(link.target)) {
                data.links.push({
                    source: node,
                    target: cache.visited.get(link.target),
                    type: link.type,
                    visited: true
                });
            }
        });
        node.links_incoming.forEach(function(link) {
            if (cache.visited.has(link.target)) {
                data.links.push({
                    source : cache.visited.get(link.target),
                    target : node,
                    type : link.type,
                    visited : true
                });
            }
        });
    };


    /**
     * Add previous visited nodes and links to the graph
     * @param node
     */
    var addPreviousVisitedNodesAndLinksToGraph = function(node) {
        var visited_nodes = cache.visited.values();
        visited_nodes.forEach(function (_node) {
            data.nodes.push(_node);
            if (_node != node) {
                _node.selected = false;
                addLinksOfVisitedNodesToGraph(_node);
            }
        });
    };

    /**
     * Add new nodes to graph
     * @param nodes Array of node objects
     */
    var addNewNodesToGraph = function(nodes) {
        var limit = (selected_node) ? selected_node.n_clicks * paging : 9999;
        var i = 0;
        for (var k in nodes) {
            var _node = nodes[k];
            // Only add 'new' nodes, visited were added before
            if (!cache.visited.has(_node.properties.id)) {
                cache.nodes.set(_node.properties.id, _node);
                if (i <= limit) {
                    // Check that we don't exceed the limit
                    data.nodes.push(_node);
                }
                i++;
            }
        }
    };


    /**
     *Add new links to graph
     * @param links Array of link objects
     */
    var addNewLinksToGraph = function(links) {
        var i = 0;
        var limit = (selected_node) ? selected_node.n_clicks * paging : 9999;
        for (var k in links) {
            var link = links[k];
            if (i <= limit) {
                var source = cache.visited.has(link.source) ? cache.visited.get(link.source) : cache.nodes.get(link.source);
                var target = cache.visited.has(link.target) ? cache.visited.get(link.target) : cache.nodes.get(link.target);
                data.links.push({
                    'source': source,
                    'target': target,
                    'type': link.type
                });
            }

            // Add all outgoing and incoming links to the current selected node
            if (selected_node && selected_node.n_clicks == 1) {
                var l = {
                    type : link.type
                };
                if (link.source == selected_node.properties.id) {
                    l.target = link.target;
                    selected_node.links_outgoing.push(l);
                } else {
                    l.target = link.source;
                    selected_node.links_incoming.push(l);
                }
            }
            i++;
        }
    };


    /**
     * Request new data from server
     * @param node_clicked JSON object of node
     */
    var getNewData = function(node_clicked) {

        $(document).trigger('onLoadData');

        $.ajax({
            url: fetch_urls[node_clicked.type] + node_clicked.properties.q,
            success: function (response) {
                setData(response, node_clicked);
            }
        });

    };

    /**
     * Render left sidebar with mustache templates
     * @param node
     */
    var renderLeftSidebar = function(node) {

        console.log(node);
        if (node.type == 'user') {
            renderUser(node);
        } else {
            renderRepository(node);
        }

    };



    var renderUser = function(node) {

        $.get('templates/user.html', function(template) {

            var rendered = Mustache.render(template, node);
            $('#ge-left-sidebar').html(rendered);

        });

    };

    var renderRepository = function(node) {

        $.get('templates/repository.html', function(template) {

            var rendered = Mustache.render(template, node);
            $('#ge-left-sidebar').html(rendered);

        });

    };


    /**
     * Set new data (internal)
     * @param data
     * @param node_clicked
     */
    var setData = function(data, node_clicked) {

        console.log(data);

        if (!selected_node) {
            node_clicked.properties.id = data.nodes[0].properties.id;
            //clicked_first_node = true;
        }

        // The first node in the data set is equal to the clicked one
        // Force same position and initialize some properties
        if (!cache.visited.has(node_clicked.properties.id)) {
            var x = node_clicked.x;
            var y = node_clicked.y;
            var node = data.nodes[0];
            node.fixed = true;
            node.x = x;
            node.y = y;
            node.visited = true;
            node.n_clicks = 1;
            node.links_incoming = [];
            node.links_outgoing = [];
            node.selected = true;
        } else {
            node = cache.visited.get(node_clicked.properties.id);
            node.n_clicks++;
        }

        // Store the selected node
        selected_node = node;

        // Reset all the previous loaded nodes
        data.nodes.splice();

        // Store node in visited cache
        cache.visited.set(node.properties.id, node);

        // Add all previous visited nodes and links to the graph
        addPreviousVisitedNodesAndLinksToGraph(node);

        // Add new nodes to graph according to paging
        addNewNodesToGraph(data.nodes);

        // Remove old links
        data.links.splice();

        // Add new links to graph according to paging
        addNewLinksToGraph(data.links);

        // Display stuff in left sidebar and refresh the graph
        if (node.n_clicks == 1) {
            addDetailInfosToNode(node);
        }
        renderLeftSidebar(node);
        refreshGraph();

        //Notify listeners
        $(document).trigger('onDataLoaded');
    };


    // Public functions


    /**
     * Render the graph
     *
     */
    var refreshGraph = function() {

        // ************************************************
        // 1) LINKS
        // *************************************************
        link = link.data(force.links());

        // Remove updated link data
        link.remove();

        // New link data
        link.enter()
            .insert('line', '.node')
            // Only the links going to the clicked node are flagged as active
            .attr('class', function (l) {
                //var _class = (l.source == node_clicked) ? 'active' : 'inactive';
                var visited = l.visited ? ' visited' : '';
                return 'link ' + l.type + visited;
            })
            .attr('marker-end', function (l) {
                return 'url(#' + l.type + ')';
            })
            .attr('stroke-dasharray', function (l) {
                return l.type == 'contributes' ? '5,5' : '';
            });

        // Remove removed link data
        link.exit().remove();


        // ************************************************
        // 2) NODES
        // *************************************************
        node = node.data(force.nodes());

        // Remove updated node data
        node.remove();

        // New node data
        var g = node.enter()
            .append('g')
            .attr("class", function (d) {
                var visited = d.visited ? ' visited' : '';
                var selected = d.selected ? ' selected' : '';
                return 'node active ' + d.type + visited + selected;
            })
            .attr('data-id', function(d) {
                return d.properties.id;
            })
            .on('click', onNodeClick)
            .call(force.drag);
        g.append("circle").attr("r", radius);
        g.append('text')
            .attr('class', 'label')
            .attr('x', 0)
            .attr('dy', '2em')
            .attr('text-anchor', 'middle')
            .text(function (d) {
                return d.label
            });

        // Removed node data
        node.exit().remove();

        force.start();

        // Send event
        $(document).trigger('onFinishedRendering');
    };


    /**
     * Init app
     */
    var init = function(_width, _height, _radius) {
        width = _width;
        height = _height;
        radius = _radius;
        initD3s();
    };


    /**
     * Set node data
     *
     * @param node_data
     */
    var setInitialNodeData = function(node_data) {
        node_data.x = width/2 - radius/2;
        node_data.y = height/2 - radius/2;
        node_data.fixed = true;
        data.nodes.push(node_data);
    };


    /**
     * Return the current selected node
     * @returns node object
     */
    var getSelectedNode = function() {
        return selected_node;
    };


    /**
     * Reset app by clearing caches and resetting variables
     */
    var reset = function() {
        //clicked_first_node = false;
        data.nodes.splice();
        data.links.splice();
        node.remove();
        link.remove();
        cache.visited = d3.map();
        cache.nodes = d3.map();
        selected_node = null;
    };


    /**
     * Set data from external
     * @param data
     */
    var setExternalData = function(data) {

        // Reset all the previous loaded nodes
        data.nodes.splice();

        // Force first node to be centered and fixed
        data.nodes[0].x = width/2 - radius/2;
        data.nodes[0].y = height/2 - radius/2;
        data.nodes[0].fixed = true;

        // Add new nodes to graph according to paging
        addNewNodesToGraph(data.nodes);

        // Remove old links
        data.links.splice();

        // Add new links to graph according to paging
        addNewLinksToGraph(data.links);

    };


    // Expose public stuff
    return {
        init: init,
        reset : reset,
        refreshGraph: refreshGraph,
        setInitialNodeData: setInitialNodeData,
        getSelectedNode : getSelectedNode,
        setData : setExternalData
    };

}($, d3));


/**
 * Setup semantic ui
 * @param $ Locally scoped JQuery object
 */
var setupUI = function($) {

    $('.ui.sidebar').sidebar();

    $('#ge-zones .button').popup({
        on: 'hover',
        position: 'bottom left'
    });

    $('.ui.accordion').accordion();
};

/**
 * Setup event listeners
 * @param $ Locally scoped JQuery object
 */
var setupEvents = function($) {

    $('.ui.checkbox').checkbox({
        'fireOnInit' : false,
        'onChecked' : function() {
            var name = $(this).attr('name');
            if (name == 'user') {
                filterUser('show');
            } else {
                filterRepository('show');
            }
        },
        'onUnchecked' : function() {
            var name = $(this).attr('name');
            if (name == 'user') {
                filterUser('hide');
            } else {
                filterRepository('hide');
            }
        }
    });

    $('#ge-zones a').on('click', function() {
        var $elem = $(this);
        $elem.parent().find('a').removeClass('active');
        $elem.addClass('active');

        if ($elem.attr('href') == '#explore-zone') {
            initExploreZone();
        } else {
            initQueryZone();
        }

    });

    $('#ge-explore').on('click', function() {
        var $input = $(this).prev().find('input');
        var username = $input.val();
        if (!username) {
            $input.closest('.field').addClass('error');
            return false;
        } else {
            renderExploreZone(username);
        }
    });

    $('#ge-modal-feedback').modal({
        onApprove : function() {
            var feedback = $(this).find('textarea').val();
            sendFeedback(feedback);
        }
    });

    $('#ge-modal-help').modal();

    $('#ge-feedback').on('click', function() {
        $('#ge-modal-feedback').modal('show');
    });

    $('#ge-help').on('click', function () {
        $('#ge-modal-help').modal('show');
    });

    $(document).on('onLoadData', function() {
        $('#loader').addClass('active');
        $('#ge-left-sidebar').fadeIn();
        $('#ge-explore-info').fadeOut();
    });

    $(document).on('onFinishedRendering', function() {
        // Update filters
        if ($('#ge-filter-user').checkbox('is unchecked')) filterUser('hide');
        if ($('#ge-filter-repository').checkbox('is unchecked')) filterRepository('hide');
        $('#loader').removeClass('active');
    });

    $('button.ge-query').click(function(e) {
        e.preventDefault();
        var $form = $(this).closest('form');
        var data = $form.serialize();
        var url = $form.attr('action');
        $('#loader').addClass('active');
        $.get(url, data, function(response) {
            console.log(response);
            if (response.nodes.length) {
                github_explorer.reset();
                github_explorer.setData(response);
                github_explorer.refreshGraph();
                $('#ge-query-info').fadeOut();
            }
            $('#loader').removeClass('active');
        });
    });

    $('.ge-clone-field').on('click', function() {
        var $field = $(this).parent().prev('.field');
        $field.clone().insertBefore($(this));
    });

    $(document).on({
        mouseenter : function() {
            $(this).find('.ge-link').show();
            //var id = $(this).attr('data-id');
            //$('g[data-id=' + id + ']').attr('class', function(index, classNames) {
            //    return classNames + ' hover';
            //});
        },
        mouseleave : function() {
            $(this).find('.ge-link').hide();
            //var id = $(this).attr('data-id');
            //$('g[data-id="' + id + '"]').attr('class', function(index, classNames) {
            //   return classNames.replace('hover', '');
            //});
        }
    }, '#ge-left-sidebar .ge-node');
};

var filterRepository = function(mode) {

    if ( ! github_explorer.getSelectedNode()) return;

    var type = github_explorer.getSelectedNode().type;

    if (mode == 'hide') {
      switch (type) {
          case 'user':
              $('g.repository').not('.visited').fadeOut();
              $('line.owns, line.contributes, line.stars').not('.visited').fadeOut();
              break;
      }
  } else {
      $('g.repository').show();
      $('line.owns, line.contributes, line.stars').fadeIn();
  }

};


var filterUser = function(mode) {

    if ( ! github_explorer.getSelectedNode()) return;

    var type = github_explorer.getSelectedNode().type;

    if (mode == 'hide') {
        $('g.user').not('.visited').fadeOut();
        switch (type) {
            case 'user':
                $('line.follows').not('.visited').fadeOut();
                break;
            case 'repository':
                $('line.stars, line.owns, line.contributes').not('.visited').fadeOut();
                break;
        }
    } else {
        $('g.user').show();
        switch (type) {
            case 'user':
                $('line.follows').not('.visited').fadeIn();
                break;
            case 'repository':
                $('line.stars, line.owns, line.contributes').fadeIn();
                break;
        }
    }

};


var sendFeedback = function(message) {

    $.ajax({
        url: '/contact',
        type: 'POST',
        data: JSON.stringify({body: message}),
        contentType: 'application/json',
        complete: function (response) {
            if (response && response.success) {
                alert('Thanks for your feedback!');
            } else {
                alert('Sorry, something went wrong sending the feedback');
            }
        }
    });
};


var initZones = function() {
    var $zones = $('#ge-zones');
    $zones.css('left', 'auto')
        .css('top', '25px')
        .css('right', '25px')
        .removeClass('massive')
        .find('p').remove();
    $('#ge-filters').fadeIn();
    $('#ge-left-sidebar').html('').fadeOut();
    $('#ge-legend').fadeIn();
};


/**
 * Start explore view
 */
var initExploreZone = function() {
    //$('#ge-explore-zone-modal').modal('show');
    initZones();
    $('#ge-explore-info').fadeIn();
    $('#ge-query-info').fadeOut();
};

/**
 * Start rendering explore zone, triggered if user entered the username in the modal
 * @param username Initial GitHub username
 */
var renderExploreZone = function(username) {

    var initial_node_data = {
        type: "user",
        label: username,
        properties: {
            q: username,
            id: 0
        }
    };

    $('#ge-explore-info').fadeOut();
    github_explorer.reset();
    github_explorer.setInitialNodeData(initial_node_data);
    github_explorer.refreshGraph();
};


var initQueryZone = function() {
    initZones();
    github_explorer.reset();
    github_explorer.refreshGraph();
    $('#ge-explore-info').fadeOut();
    $('#ge-query-info').fadeIn();
};



// Main call when DOM is ready
$(function() {

    var width = $(window).width();
    var height = $(window).height();
    var radius = 15;

    github_explorer.init(width, height, radius);

    setupUI($);
    setupEvents($);

});