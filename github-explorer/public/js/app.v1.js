
var github_explorer = (function ($) {

    // Private variables

    var width;
    var height;
    var radius = 15;
    var fetch_user_url = '/users?q=';
    var fetch_repository_url = '/repositories?q=';
    var data = {
        "nodes" : [],
        'links' : []
    };
    var cache = {};
    cache.lookup_table = [];
    cache.visited = [];
    var force;
    var svg;
    var node;
    var link;

    // Private functions

    /**
     * Initialize D3s
     */
    var initD3s = function() {
        force = d3.layout.force()
            .friction(0.8)
            .linkDistance(200)
            .gravity(0.0)
            .charge(-250)
            .chargeDistance(250)
            .size([width, height])
            .nodes(data.nodes)
            .links(data.links)
            .on('tick', tick);

        svg = d3.select('#ge-explore')
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox', '0 0 ' + width + ' ' + height);

        node = svg.selectAll('.node');
        link = svg.append('svg:g').selectAll('.link');
    };


    /**
     * Update graph simulation
     */
    function tick() {
        node.attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
        link.attr("x1", function (d) {
            return d.source.x;
        })
            .attr("y1", function (d) {
                return d.source.y;
            })
            .attr("x2", function (d) {
                return d.target.x;
            })
            .attr("y2", function (d) {
                return d.target.y;
            });
    }


    /**
     * Refresh graph
     *
     * @param node_clicked Node object that was clicked
     * @param new_data Boolean if new data was requested
     */
    var refreshGraph = function(node_clicked, new_data) {

        // 1) LINKS //

        link = link.data(force.links());

        var active_links = [];

        // Update link data
        link.attr('class', function (l) {
            // Set class to inactive if new data was loaded, otherwise if node was clicked and data already loaded, flag as active
            var _class = (new_data == false && l.source == node_clicked) ? 'active' : 'inactive';
            if (_class == 'active') {
                active_links.push(l);
            }
            return 'link ' + _class + ' ' + l.type;
        }).attr('marker-end', function (l) {
            var $link = d3.select(this);
            return ($link.classed('inactive')) ? 'url(#inactive)' : 'url(#' + l.type + ')';
        });

        // New link data
        link.enter()
            .insert('line', '.node')
            // Only the links going to the clicked node are flagged as active
            .attr('class', function (l) {
                var _class = (l.source == node_clicked) ? 'active' : 'inactive';
                return 'link ' + _class + ' ' + l.type;
            })
            .attr('marker-end', function (l) {
                return 'url(#' + l.type + ')';
            })
            .attr('stroke-dasharray', function(l) {
                return l.type == 'contributes' ? '5,5' : '';
            });

        // Removed link data
        link.exit().remove();


        // 2) NODES //

        node = node.data(force.nodes());

        // Update node data
        node.attr('class', function (d) {
            var _class = 'inactive';
            for (var i = 0; i < active_links.length; i++) {
                if (d == active_links[i].target) {
                    _class = 'active';
                    break;
                }
            }
            return 'node ' + _class + ' ' + d.type;
        });

        // New node data
        var g = node.enter()
            .append('g')
            .attr("class", function (d) {
                return 'node active ' + d.type;
            })
            .on('click', onNodeClick)
            .on('dblclick', onNodeDblClick)
            .on('mouseover', onNodeHover)
            .on('mouseleave', onNodeMouseOut)
            .call(force.drag);
        g.append("circle").attr("r", radius);
        g.append('text')
            .attr('class', 'label')
            .attr('x', 0)
            .attr('dy', '2em')
            .attr('text-anchor', 'middle')
            .text(function (d) {
                return d.label
            });

        // Removed node data
        node.exit().remove();

        force.start();
    };

    /**
     * Highlight selected node
     * @param $node
     */
    var highlight = function ($node) {
        $node.classed('selected active', true);
        $node.classed('inactive', false);
        $node.select('circle').attr('r', radius * 1.1);
    };

    // Handler functions

    /**
     * On Node click
     * @param node
     */
    var onNodeClick = function(node) {
        if (d3.event.defaultPrevented) return; // ignore drag

        var $node = d3.select(this);

        //console.log('clicked node ' + node);
        node.fixed = true;
        if (cache.visited.indexOf(node.index) == -1) {
            getNewData(node, $node);
        } else {
            refreshGraph(node, false);
        }

        highlight($node);
    };

    /**
     * On Node double-click
     * @param node
     */
    var onNodeDblClick = function (node) {
        var box_coordinates = svg.attr('viewBox').split(' ');
        var x = node.x - (width / 2 - radius);
        var y = node.y - (height / 2 - radius);
        svg.attr('viewBox', x + ' ' + y + ' ' + box_coordinates[2] + ' ' + box_coordinates[3]);
    };

    /**
     * Hover over node
     * @param node
     */
    var onNodeHover = function (node) {
        var $node = d3.select(this);
        $node.classed($node.attr('class') + ' hover', true);
    };


    /**
     * Leave node
     * @param node
     */
    var onNodeMouseOut = function (node) {
        var $node = d3.select(this);
        $node.classed('hover', false);
    };


    /**
     * Request new data from server
     * @param node JSON object of node
     * @param $node Jquery object of node
     */
    var getNewData = function(node, $node) {

        var url = (node.type == 'user') ? fetch_user_url : fetch_repository_url;

        $('#loader').addClass('active');

        $.ajax({
            url: url + node.q,
            success: function (response) {

                console.log(response);

                // TODO This can be solved with a status
                if ( ! cache.lookup_table.length) {
                    cache.lookup_table[response.links[0].source] = 0;
                }


                for (var k in response.nodes) {
                    var n = response.nodes[k];
                    var id = n.properties.id;

                    if (id in cache.lookup_table) {
                        // We already loaded the node before... nothing to do!
                    } else {
                        var _node = n;
                        _node.label = n.label;
                        _node.type = n.type;
                        _node.properties = n.properties;
                        _node.q = n.properties.q;
                        cache.lookup_table[id] = data.nodes.length;
                        data.nodes.push(_node)
                    }

                }

                for (var k in response.links) {
                    var l = response.links[k];
                    var type = (l.type == 'stars') ? 'starred' : l.type;
                    data.links.push({
                        'source': data.nodes[cache.lookup_table[l.source]],
                        'target': data.nodes[cache.lookup_table[l.target]],
                        'type': type
                    });
                }

                //console.log(data);
                //console.log(lookup_table);

                // TODO Display properties in sidebar with moustache templates

                var $info = $('#ge-info');
                $info.find('h2').html('<i class="user icon"></i> ' + node.label);
                //if ( ! $info.sidebar('is visible')) {
                //    $info.sidebar({dimPage: false, overlay: true}).sidebar('show');
                //}

                refreshGraph(node, true);
                cache.visited.push(node.index);

                highlight($node);

                $('#loader').removeClass('active');

            }
        });

    };


    // Public functions


    /**
     * Init app
     */
    var init = function(_width, _height, _radius) {
        width = _width;
        height = _height;
        radius = _radius;
        initD3s();
    };


    /**
     * Set node data
     *
     * @param node_data
     */
    var setNodeData = function(node_data) {
        data.nodes.push(node_data);
    };


    // Expose public stuff
    return {
        width : width,
        height : height,
        radius : radius,
        init : init,
        refreshGraph : refreshGraph,
        setNodeData : setNodeData
    };

}($));


$(document).ready(function () {

    var width = $(window).width();
    var height = $(window).height();
    var radius = 15;

    github_explorer.init(width, height, radius);

    $('.ui.sidebar').sidebar();


    $('#ge-zones .button').popup({
        on: 'hover',
        position: 'bottom left'
    });

    $('#ge-modal-start').modal('show');

    $('#ge-start').click(function () {
        var username = $(this).parent().prev('.content').find('input').val();
        var initial_node_data = {
            "type": "user",
            "label": username,
            "q": username,
            'x': github_explorer.width / 2 - github_explorer.radius,
            'y': github_explorer.height / 2 - github_explorer.radius
        };
        github_explorer.setNodeData(initial_node_data);
        github_explorer.refreshGraph();
    });

});