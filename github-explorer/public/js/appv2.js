var Data = function () {

    var data = {
        'nodes': [],
        'links': []
    };

    var setNodeData = function (node_data) {
        data.nodes.push(node_data);
    };

    var setLinkData = function (link_data) {
        data.links.push(link_data);
    };

};


var github_explorer = (function ($, d3) {

    // Private variables

    var width;
    var height;
    var radius = 15;
    var fetch_user_url = '/users?q=';
    var fetch_repository_url = '/repositories?q=';
    var data = {
        "nodes": [],
        'links': []
    };
    var cache = {};
    var clicked_first_node = false;
    //cache.lookup_table = [];
    cache.visited = d3.map();
    cache.nodes = d3.map();
    cache.links = d3.map();

    // Store node in cache
    //cache.storeNode = function (node) {
    //    cache.nodes[node.properties.id] = node;
    //    //if (node.properties.id in cache.visited) {
    //    //    //cache.nodes[node.properties.id] = node;
    //    //} else {
    //    //    cache.nodes[node.properties.id] = node;
    //    //}
    //};
    //
    //cache.storeNodes = function (nodes) {
    //    for (var k in nodes) {
    //        cache.storeNode(nodes[k]);
    //    }
    //};
    //
    //cache.getNode = function (github_node_id) {
    //    var node = cache.nodes[github_node_id];
    //    //if (node) {
    //    //    node.x = undefined;
    //    //    node.y = undefined;
    //    //    node.fixed = false;
    //    //}
    //    return node;
    //};


    var force;
    var svg;
    var node;
    var link;

    // Private functions

    /**
     * Initialize D3s
     */
    var initD3s = function () {
        force = d3.layout.force()
            .friction(0.8)
            .linkDistance(200)
            .gravity(0.0)
            .charge(-250)
            .chargeDistance(250)
            .size([width, height])
            .nodes(data.nodes)
            .links(data.links)
            .on('tick', tick);

        svg = d3.select('#ge-explore')
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox', '0 0 ' + width + ' ' + height);

        node = svg.selectAll('.node');
        link = svg.append('svg:g').selectAll('.link');
    };


    /**
     * Update graph simulation
     */
    function tick() {
        node.attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
        link.attr("x1", function (d) {
            return d.source.x;
        })
            .attr("y1", function (d) {
                return d.source.y;
            })
            .attr("x2", function (d) {
                return d.target.x;
            })
            .attr("y2", function (d) {
                return d.target.y;
            });
    }


    /**
     * Refresh graph
     *
     * @param node_clicked Node object that was clicked
     * @param new_data Boolean if new data was requested
     */
    var refreshGraph = function (node_clicked, new_data) {

        // 1) LINKS //

        link = link.data(force.links());

        var active_links = [];

        // Update link data
        //link.attr('class', function (l) {
        //    // Set class to inactive if new data was loaded, otherwise if node was clicked and data already loaded, flag as active
        //    var _class = (new_data == false && l.source == node_clicked) ? 'active' : 'inactive';
        //    if (_class == 'active') {
        //        active_links.push(l);
        //    }
        //    return 'link ' + _class + ' ' + l.type;
        //}).attr('marker-end', function (l) {
        //    var $link = d3.select(this);
        //    return ($link.classed('inactive')) ? 'url(#inactive)' : 'url(#' + l.type + ')';
        //});

        link.remove();

        // New link data
        link.enter()
            .insert('line', '.node')
            // Only the links going to the clicked node are flagged as active
            .attr('class', function (l) {
                //var _class = (l.source == node_clicked) ? 'active' : 'inactive';
                return 'link ' + l.type;
            })
            .attr('marker-end', function (l) {
                return 'url(#' + l.type + ')';
            })
            .attr('stroke-dasharray', function (l) {
                return l.type == 'contributes' ? '5,5' : '';
            });

        // Removed link data
        link.exit().remove();


        // 2) NODES //
        node = node.data(force.nodes());

        // Update node data
        //node.attr('class', function (d) {
        //    var _class = 'inactive';
        //    for (var i = 0; i < active_links.length; i++) {
        //        if (d == active_links[i].target) {
        //            _class = 'active';
        //            break;
        //        }
        //    }
        //    return 'node ' + _class + ' ' + d.type;
        //});

        node.remove();

        // New node data
        var g = node.enter()
            .append('g')
            .attr("class", function (d) {
                var visited = d.visited ? ' selected' : '';
                return 'node active ' + d.type + visited;
            })
            .on('click', onNodeClick)
            .on('dblclick', onNodeDblClick)
            .on('mouseover', onNodeHover)
            .on('mouseleave', onNodeMouseOut)
            .call(force.drag);
        g.append("circle").attr("r", radius);
        g.append('text')
            .attr('class', 'label')
            .attr('x', 0)
            .attr('dy', '2em')
            .attr('text-anchor', 'middle')
            .text(function (d) {
                return d.label
            });

        // Removed node data
        node.exit().remove();

        force.start();
    };

    /**
     * Highlight selected node
     * @param $node
     */
    var highlight = function ($node) {
        $node.classed('selected active', true);
        $node.classed('inactive', false);
        $node.select('circle').attr('r', radius * 1.1);
    };

    // Handler functions

    /**
     * On Node click
     * @param node
     */
    var onNodeClick = function (node) {
        if (d3.event.defaultPrevented) return; // ignore drag

        var $node = d3.select(this);

        //console.log('clicked node ' + node);
        //node.fixed = true;
        //if (cache.visited.indexOf(node.index) == -1) {
        //    getNewData(node, $node);
        //} else {
        //    refreshGraph(node, false);
        //}

        getNewData(node);

        highlight($node);
    };

    /**
     * On Node double-click
     * @param node
     */
    var onNodeDblClick = function (node) {
        var box_coordinates = svg.attr('viewBox').split(' ');
        var x = node.x - (width / 2 - radius);
        var y = node.y - (height / 2 - radius);
        svg.attr('viewBox', x + ' ' + y + ' ' + box_coordinates[2] + ' ' + box_coordinates[3]);
    };

    /**
     * Hover over node
     * @param node
     */
    var onNodeHover = function (node) {
        var $node = d3.select(this);
        $node.classed($node.attr('class') + ' hover', true);
    };


    /**
     * Leave node
     * @param node
     */
    var onNodeMouseOut = function (node) {
        var $node = d3.select(this);
        $node.classed('hover', false);
    };


    /**
     * Request new data from server
     * @param node_clicked JSON object of node
     * @param $node Jquery object of node
     */
    var getNewData = function (node_clicked, $node) {

        // First of all, check if already visited and data can be loaded from cache
        //if (node.properties.id in cache.visited) {
        //    _node = cache.getNode(node.properties.id);
        //    data.nodes.splice();
        //    for (var k in _node.nodes) {
        //        data.nodes.push(_node.nodes[k]);
        //    }
        //    data.links.splice();
        //    for (var k in _node.links_outgoing) {
        //        var link = _node.links_outgoing[k];
        //        var type = (link.type == 'stars') ? 'starred' : link.type;
        //        data.links.push({
        //            'source': cache.getNode(link.source),
        //            'target': cache.getNode(link.target),
        //            'type': type
        //        });
        //    }
        //
        //    refreshGraph(node, true);
        //
        //} else {

            var url = (node_clicked.type == 'user') ? fetch_user_url : fetch_repository_url;

            $('#loader').addClass('active');

            $.ajax({
                url: url + node_clicked.properties.q,
                success: function (response) {

                    console.log(response);

                    if ( ! clicked_first_node) {
                        node_clicked.properties.id = response.links[0].source;
                    }

                    var node_cache = d3.map();

                    // The first node in the response set is equal to the clicked one
                    // Force same position!
                    var x = node_clicked.x;
                    var y = node_clicked.y;
                    var node = response.nodes[0];
                    node.fixed = true;
                    node.x = x;
                    node.y = y;
                    node.visited = true;

                    // Add target nodes and outgoing links as property to node so we can get that info for the sidebar
                    node.links_outgoing = response.links;
                    node.nodes = response.nodes;

                    // Reset all the previous loaded nodes
                    data.nodes.splice();

                    // I'm sorry but it does work only with loops...
                    var nodes = response.nodes;
                    for (var k in nodes) {
                        var _node = nodes[k];
                        data.nodes.push(_node);
                        node_cache.set(_node.properties.id, _node);
                    }

                    console.log(data.nodes);

                    //Any nodes that were previous visited need the same position again
                    if ( ! cache.visited.empty()) {
                        cache.visited.forEach(function(id, n) {
                            if (node_cache.has(id)) {
                                var node = node_cache.get(id);
                                node.x = n.x;
                                node.y = n.y;
                                node.visited = true;
                            } else {
                                // The visited node was not loaded again, we need to create the link to the latest node
                                data.nodes.push(n);
                                for (var i in n.links_outgoing) {
                                    var link = n.links_outgoing[i];
                                    if (link.target == n.properties.id) {
                                        data.links.push({
                                            'source': n.properties.id,
                                            'target': node.properties.id,
                                            'type': link.type
                                        });
                                    }
                                }
                            }
                        });
                    }

                    //data.nodes.push(node);

                    // Store as visited node
                    cache.visited.set(node.properties.id, node);

                    //Add all previous clicked nodes again
                    //data.nodes.push(cache.visited.values());


                    //console.log(cache);
                    //console.log(data.nodes);

                    // Remove old links
                    data.links.splice();

                    for (var k in response.links) {
                        var link = response.links[k];
                        var type = (link.type == 'stars') ? 'starred' : link.type;

                        // Now it gets tricky...
                        // If we have a link to a node that was visited before but is not in the current result set, we must re-add the node and the links

                        //var source = cache.visited.has(link.source) ? cache.visited.get(link.source) : cache.nodes.get(link.source);
                        //var target = cache.visited.has(link.target) ? cache.visited.get(link.target) : cache.nodes.get(link.target);
                        data.links.push({
                            'source': node_cache.get(link.source),
                            'target': node_cache.get(link.target),
                            'type': type
                        });
                    }


                    //console.log(data);

                    //// TODO Display properties in sidebar with moustache templates
                    //
                    ////var $info = $('#ge-info');
                    ////$info.find('h2').html('<i class="user icon"></i> ' + node.label);
                    ////if ( ! $info.sidebar('is visible')) {
                    ////    $info.sidebar({dimPage: false, overlay: true}).sidebar('show');
                    ////}

                    refreshGraph(node, true);
                    //cache.visited.push(node.index);

                    //highlight($node);

                    $('#loader').removeClass('active');

                }
            });

        //}

        //console.log(cache);

    };


    // Public functions


    /**
     * Init app
     */
    var init = function (_width, _height, _radius) {
        width = _width;
        height = _height;
        radius = _radius;
        initD3s();
    };


    /**
     * Set node data
     *
     * @param node_data
     */
    var setNodeData = function (node_data) {
        data.nodes.push(node_data);
    };


    // Expose public stuff
    return {
        width: width,
        height: height,
        radius: radius,
        init: init,
        refreshGraph: refreshGraph,
        setNodeData: setNodeData
    };

}($, d3));


$(document).ready(function () {

    var width = $(window).width();
    var height = $(window).height();
    var radius = 15;

    github_explorer.init(width, height, radius);

    $('.ui.sidebar').sidebar();


    $('#ge-zones .button').popup({
        on: 'hover',
        position: 'bottom left'
    });

    $('#ge-modal-start').modal('show');

    $('#ge-start').click(function () {
        var username = $(this).parent().prev('.content').find('input').val();
        var initial_node_data = {
            type: "user",
            label: username,
            properties: {
                q: username,
                id: 0
            },
            x: width / 2 - radius,
            y: height / 2 - radius
        };
        github_explorer.setNodeData(initial_node_data);
        github_explorer.refreshGraph();
    });

});