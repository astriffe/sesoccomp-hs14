class DataController

  attr_accessor :github_proxy, :neo4j_proxy

  UP_TO_DATE_LIMIT_IN_DAYS = 5 # = 5 days


  def initialize(params={github_proxy: GithubProxy, neo4j_proxy: Neo4jProxy.new})
    # preconditions
    raise 'github_proxy is nil!' if params[:github_proxy].nil?
    raise 'neo4j_proxy is nil' if params[:neo4j_proxy].nil?

    @github_proxy = params[:github_proxy]
    @neo4j_proxy = params[:neo4j_proxy]
  end


  def find_shortest_path(q1, q2)
    @neo4j_proxy.find_shortest_path(q1,q2)
  end

  def find_paths(q1, q2, limit=25, max_length=3)
    @neo4j_proxy.find_paths(q1, q2, limit, max_length)
  end

  def find_repository_with(conditions,limit)
    @neo4j_proxy.find_repository_with(conditions,limit)
  end

  def find_random_repository(conditions)
    @neo4j_proxy.find_random_repository(conditions)
  end


  # Fetches a user from the database or from github.
  #
  # ====Examples
  # fetch_user id: 2347982
  # fetch_user login: 'wanze'
  #
  #
  def fetch_user(params)
    check_user = @neo4j_proxy.user_exists? params

    user = if check_user == false # user is not present in the database
             raise 'Error' if params[:login].nil?

             fetched_user = @github_proxy.fetch_user params[:login]

             Thread.new do
               @neo4j_proxy.add_fetched_user fetched_user
             end

             fetched_user
           elsif check_user.fetch_time + UP_TO_DATE_LIMIT_IN_DAYS < DateTime.now || check_user.full_fetch == false # user is not not up-to-date or full fetched.

             fetched_user = @github_proxy.fetch_user check_user.login
             puts 'user %s is not up-to-date or full fetched, store full fetched user in the database' % check_user.login

             Thread.new do
               @neo4j_proxy.update_fetched_user fetched_user
             end

             fetched_user
           else # otherwise user is present and up-to-date
             puts 'user %s is already stored in the database' % check_user.login
             puts "fetch time: #{check_user.fetch_time}"
             puts "test: #{check_user.fetch_time + UP_TO_DATE_LIMIT_IN_DAYS < DateTime.now}"
             @neo4j_proxy.fetch_user_by_id check_user.id
           end
    user
  end


  # Fetches a repository from the database or from github.
  #
  # ====Examples
  # fetch_repository id: 2347982
  # fetch_repository full_name: 'ruby/ruby'
  #
  def fetch_repository(params)
    check_repo = @neo4j_proxy.repository_exists? params

    repo = if check_repo == false
             raise 'Error' if params[:full_name].nil?

             # repo is not present in the database
             fetched_repo = @github_proxy.fetch_repository params[:full_name]

             Thread.new do
               @neo4j_proxy.add_fetched_repository fetched_repo
             end

             fetched_repo

           elsif check_repo.fetch_time + UP_TO_DATE_LIMIT_IN_DAYS < DateTime.now || check_repo.full_fetch == false # repo is neighbour fetched or is not up-to-date.
             fetched_repo = @github_proxy.fetch_repository check_repo.full_name

             Thread.new do
               @neo4j_proxy.update_fetched_repository fetched_repo
             end

             fetched_repo
           else # otherwise repo is present and up-to-date
             @neo4j_proxy.fetch_repository_by_id check_repo.id
           end
    repo
  end


end