module Model

  TIME_FORMAT = '%Y-%m-%d %H:%M:%S'


  def self.from_neo4j(a_hash)
    properties = a_hash.dup
    # convert string keys to symbols
    properties = Hash[ properties.map { |k,v| [k.to_sym,v] } ]
    if properties[:type] == 'User'
      UserView.from_neo4j properties
    elsif properties[:type] == 'Repository'
      RepositoryView.from_neo4j properties
    else
      raise 'Invalid type!'
    end
  end


  module ViewHash

    def self.included(base)
      base.send :include, InstanceMethods
    end

    module InstanceMethods

      # Converts this model to a json representation using the to_view_hash method.
      def to_json(options={}) #options params is necessary otherwise sinatra complains...
        JSON.pretty_generate to_view_hash, indent: '    ', space: '  '
      end

      # Converts this model to a view hash. This method is used to provide the requested hash for the view.
      def to_view_hash
        raise 'Not implemented!'
      end

    end

  end


  #
  # EmptyView for the frontend
  #
  class EmptyView
    include ViewHash

    def to_view_hash
      {
          nodes: [],
          links: []
      }
    end

  end


  #
  # ShortestPathView for the frontend
  #
  class ShortestPathView
    include ViewHash

    def initialize(a_hash)
      @nodes = a_hash[:nodes]
      @rel_labels = a_hash[:rel_labels]
      @node_labels = a_hash[:node_labels]
    end


    def to_view_hash
      view_hash = {
          nodes: [],
          links: []
      }

      @nodes.each do |a_node|
        view_hash[:nodes] << {
            type: a_node.view_type,
            label: a_node.view_label,
            properties: a_node.to_hash #.merge(q: q_property)
        }
      end

      @rel_labels.zip(@nodes[0...-1],@nodes[1..-1]).each do |r,n1,n2|
        view_hash[:links] << {
            type: r.downcase,
            source: n1.id,
            target: n2.id,
        }
      end
      view_hash
    end

  end


  #
  # BaseView for the frontend
  #
  class BaseView
    include ViewHash

    attr_accessor :properties

    def initialize(a_hash={})
      # convert string keys to symbols
      property_list = a_hash.map {|k,v| [k.to_sym,v] }
      @properties = Hash[property_list] # create a hash
    end


    # Catches all setter and getter calls.
    def method_missing(method_name, *arguments, &block)
      if method_name =~ /.*=$/ # if its a setter
        key = method_name[0...-1].to_sym # remove '='
        @properties[key] = arguments.first
      else # otherwise its a getter
        @properties[method_name]
      end
    end


    # Provides hash-like access on the model properties.
    def [](key)
      @properties[key]
    end


    # Converts this model to a flat hash which is used to store the properties in the neo4j database.
    def to_hash
      hash = @properties.dup
      # remove keys with nil values
      hash.each do |key,value|
        hash.delete(key) if value.nil?
      end

      hash[:fetch_time] = hash[:fetch_time].strftime(TIME_FORMAT)
      hash
    end


    def to_view_hash
      view_hash = {
          nodes: [{# user node: is placed on index 0
                   type: self.view_type,
                   label: self.view_label,
                   properties: to_hash.merge(q: self.q) # hack alert
                  }],
          links: []
      }

      build_view_hash(view_hash)
      eliminate_duplicates(view_hash)
    end


    def view_label
      raise 'Not implemented!'
    end


    def view_type
      raise 'Not implemented!'
    end


    def create_node_and_link(params) # hack alert
      object,  node_label,  node_type = params[:node], params[:node_label], params[:node_type]
      rel_type, q_property = params[:rel_type], params[:q_property]
      source_id, target_id =  params[:source_id], params[:target_id]
      [{
           type: node_type,
           label: node_label,
           properties: object.to_hash #.merge(q: q_property)
       },
       {
           type: rel_type,
           source: source_id,
           target: target_id
       }]
    end


    def eliminate_duplicates(view_hash)
      # apply hashing to eliminate duplicates
      new_nodes, new_links = {}, {}

      view_hash[:nodes].each do |n|
        new_nodes[n[:properties][:id]] = n unless  new_nodes[n[:properties][:id]]
      end

      view_hash[:links].each do |n|
        new_links[[n[:type],n[:source],n[:target]]] = n unless  new_links[[n[:type],n[:source],n[:target]]]
      end

      view_hash[:nodes], view_hash[:links] = new_nodes.values, new_links.values
      view_hash
    end


    # Returns the fetch time as Time object. If it's a neighboured fetch node its fetch time is set to 1970-01-01 01:00:00 +0100.
    def fetch_time
      time = @properties[:fetch_time]
      case time
        when String
          DateTime.strptime time, TIME_FORMAT
        when DateTime
          time
        else
          raise 'Fetch time error'
      end
    end


    # Converts the obtained hash or fake hash from the octokit client to a model.
    def self.from_github(fake_hash)
      raise 'Not implemented!'
    end


    # Converts the obtained hash from the Neo4jProxy to a model.
    def self.from_neo4j(a_hash)
      properties = a_hash.dup
      # convert string keys to symbols
      properties = Hash[ properties.map { |k,v| [k.to_sym,v] } ]
      properties[:fetch_time] = DateTime.strptime(properties[:fetch_time], TIME_FORMAT)
      new properties
    end

  end


  class MultiplePathView < BaseView

    include ViewHash

    def initialize(neo4j_table)
      @views = []
      neo4j_table.each do |row|
        nodes = row[0]
        nodes = nodes.map {|a_hash| Model.from_neo4j a_hash}
        node_labels = row[1]
        rel_labels = row[2]
        view = Model::ShortestPathView.new nodes: nodes, node_lables: node_labels, rel_labels: rel_labels
        @views << view
      end
    end

    def to_view_hash
      view_hash = {
          nodes: [],
          links: []
      }

      @views.each do |view|
        a_hash = view.to_view_hash
        view_hash[:nodes] += a_hash[:nodes]
        view_hash[:links] += a_hash[:links]
      end

      eliminate_duplicates(view_hash)
    end

  end


  class ListView < BaseView
    include ViewHash

    def initialize(nodes)
      @nodes = nodes
    end

    def to_view_hash
      nodes = @nodes.map do |a_node|
        {
            type: a_node.view_type,
            label: a_node.view_label,
            properties: a_node.to_hash
        }
      end

      view_hash = {
          nodes: nodes,
          links: []
      }
      eliminate_duplicates(view_hash)
    end

  end


  #
  # RepositoryView for the frontend
  #
  class RepositoryView < BaseView

    def self.from_neo4j(a_hash)
      super(a_hash)
    end

    def view_label
      self.name
    end


    def view_type
      :repository
    end


    def q
      self.full_name
    end


    def self.from_github(a_hash)
      properties = a_hash.is_a?(Hash) ? a_hash.dup : a_hash.to_hash
      properties[:q] = properties[:full_name]
      properties[:full_fetch] ||= false
      properties[:owner] = properties[:owner][:login]
      properties[:type] = 'Repository'
      properties[:fetch_time] ||= DateTime.now

      begin
        org_name = properties[:organization][:login]
        properties[:organization] = org_name
      rescue
        # do nothing
      end

      new properties
    end


    # Builds a flat hash
    def to_hash
      hash = super
      hash.delete(:contributors)
      hash.delete(:stargazers)
      hash
    end


    def build_view_hash(view_hash)
      self.contributors.each do |user|
        node, link = create_node_and_link(node: user, node_label: user.login, node_type: :user, rel_type: :contributes,
                                          source_id: user.id, target_id: self.id, q_property: user.login)
        view_hash[:nodes] << node
        view_hash[:links] << link
      end

      self.stargazers.each do |user|
        node, link = create_node_and_link(node: user, node_label: user.login, node_type: :user, rel_type: :stars,
                                          source_id: user.id, target_id: self.id, q_property: user.login)
        view_hash[:nodes] << node
        view_hash[:links] << link
      end
    end

  end


  #
  # UserView for the frontend
  #
  class UserView < BaseView

    def self.from_neo4j(a_hash)
      super(a_hash)
    end


    def self.from_github(a_hash)
      properties = a_hash.is_a?(Hash) ? a_hash.dup : a_hash.to_hash
      properties[:q] = properties[:login]
      properties[:full_fetch] ||= false
      properties[:fetch_time] ||= DateTime.now
      new properties
    end

    def view_label
      self.login
    end

    def view_type
      :user
    end

    def q
      self.login
    end

    # Builds a flat hash
    def to_hash
      hash = super # calls BaseAdapter#to_hash
      # remove these properties, we don't want to pass them to the neo4j connector
      hash.delete(:repositories)
      hash.delete(:followers)
      hash.delete(:starred)
      hash.delete(:following)
      hash
    end


    def build_view_hash(view_hash)
      self.following.each do |user|
        node, link = create_node_and_link(node: user, node_label: user.login, node_type: :user, rel_type: :follows,
                                          source_id: self.id, target_id: user.id, q_property: user.login)
        view_hash[:nodes] << node
        view_hash[:links] << link
      end

      self.repositories.each do |repo|
        node, link = create_node_and_link(node: repo, node_label: repo.name, node_type: :repository, rel_type: :owns,
                                          source_id: self.id, target_id: repo.id, q_property: repo.full_name)
        view_hash[:nodes] << node
        view_hash[:links] << link
      end

      self.starred.each do |repo|
        node, link = create_node_and_link(node: repo, node_label: repo.name, node_type: :repository, rel_type: :stars,
                                          source_id: self.id, target_id: repo.id, q_property: repo.full_name)
        view_hash[:nodes] << node
        view_hash[:links] << link
      end
    end

  end

end
