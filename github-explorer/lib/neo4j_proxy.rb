class Neo4jProxy

  def initialize(connector=Neo4jConnector.new)
    @neo4j_connector = connector
  end

  def method_missing(method, *args)
    json = @neo4j_connector.send method, *args
    nodes = Neo4jResult.from_json json

    if block_given?
      yield nodes
    else
      nodes
    end
  end

  def query(query_string)
    json = @neo4j_connector.query query_string
    Neo4jResult.from_json json
  end



  # Find methods

  def find_shortest_path(q1, q2)
    json = @neo4j_connector.find_shortest_path(q1, q2)
    table = Neo4jResult.from_json json
    if table.empty?
      Model::EmptyView.new
    else
      nodes = table[0,0]
      nodes = nodes.map {|a_hash| Model.from_neo4j a_hash}
      node_labels = table[0,1]
      rel_labels = table[0,2]
      Model::ShortestPathView.new nodes: nodes, node_lables: node_labels, rel_labels: rel_labels
    end
  end

  def find_paths(q1, q2, limit=25, max_length=3)
    json = @neo4j_connector.find_paths(q1, q2, limit, max_length)
    table = Neo4jResult.from_json json
    if table.empty?
      Model::EmptyView.new
    else
      Model::MultiplePathView.new table
    end
  end


  def find_repository_with(conditions,limit)
    json = @neo4j_connector.find_repository_with(conditions,limit)
    table = Neo4jResult.from_json json
    if table.empty?
      Model::EmptyView.new
    else
      repo_hashes = table[:*,0]
      repos = repo_hashes.map {|a_hash| Model::RepositoryView.from_neo4j a_hash}
      Model::ListView.new repos
    end
  end


  def find_random_repository(conditions)
    json = @neo4j_connector.find_random_repository(conditions)
    table = Neo4jResult.from_json json
    if table.empty?
      Module::EmptyView.new
    else
      repo_hash = table[0,0]
      contributor_table = Neo4jResult.from_json @neo4j_connector.get_ingoing_relationship repo_hash['id'], 'Contributes'
      stargazer_table = Neo4jResult.from_json @neo4j_connector.get_ingoing_relationship repo_hash['id'], 'Stars'
      repo_hash['contributors'] = contributor_table[:*,1].map { |u| Model::UserView.from_neo4j u}
      repo_hash['stargazers'] = stargazer_table[:*,1].map { |u| Model::UserView.from_neo4j u}

      Model::RepositoryView.from_neo4j repo_hash
    end
  end


  # Exists methods.

  #
  # Checks if a user node exists in the database.
  #
  # ====Examples
  # user_exists? id: 327423
  # user_exists? login: 'wanze'
  #
  def user_exists?(params)
    user_id, login = params[:id], params[:login]
    json = if login
             @neo4j_connector.exist_user_node login
           elsif user_id
             @neo4j_connector.exist_user_node_ghid user_id
           else
             raise 'Error'
           end
    table = Neo4jResult.from_json json
    if table.empty?
      false
    else
      Model::UserView.from_neo4j table[0,0]
    end
  end

  #
  # Checks if a repository node exists in the database.
  #
  # ====Examples
  # repository_exists? id: 327423
  # repository_exists? full_name: 'ruby/ruby'
  #
  def repository_exists?(params)
    repo_id, full_name = params[:id], params[:full_name]
    json = if full_name
             @neo4j_connector.exist_repository_node full_name
           elsif repo_id
             @neo4j_connector.exist_repository_node_ghid repo_id
           else
             raise 'Error'
           end

    table = Neo4jResult.from_json json
    if table.empty?
      false
    else
      Model::RepositoryView.from_neo4j table[0,0]
    end
  end

  #
  # Checks if a relationship exists between two objects.
  #
  # ====Examples
  # repository_exists? label: 'Owns', first_node: a user object, second_node: a repository object.
  # repository_exists? full_name: 'ruby/ruby'
  #
  def relationship_exists?(params)
    label,node1,node2 = params[:label], params[:first_node], params[:second_node]
    json = @neo4j_connector.get_relationships_between_nodes node1.id, node2.id, label
    table = Neo4jResult.from_json json
    !table.empty?
  end

  # User methods.

  def fetch_user_by_id(id)
    json = @neo4j_connector.get_user_node_ghid id
    table = Neo4jResult.from_json json
    if table.empty?
      nil
    else
      user_hash = table[0,0] #first row, first column

      starred_table = Neo4jResult.from_json @neo4j_connector.get_outgoing_relationship(user_hash['id'], 'Stars')
      user_hash['starred'] = starred_table[:*,1].map {|u| Model::RepositoryView.from_neo4j u }

      repo_table = Neo4jResult.from_json @neo4j_connector.get_outgoing_relationship(user_hash['id'], 'Owns')

      # group nodes to eliminate duplicated nodes
      user_hash['repositories'] = repo_table[:*,1].map { |u| Model::RepositoryView.from_neo4j u}

      follower_table = Neo4jResult.from_json @neo4j_connector.get_outgoing_relationship(user_hash['id'], 'Follows')
      # group nodes to eliminate duplicated nodes

      user_hash['following'] = follower_table[:*,1].map { |u| Model::UserView.from_neo4j u}
      Model::UserView.from_neo4j user_hash
    end
  end


  # Adds a fully fetched user without checking if the user is already present.
  def add_fetched_user(user)
    @neo4j_connector.add_user_node user.to_hash
    user.followers.each do |follower|
      add_user follower
      create_follows_relationship followed: user, follower: follower
    end

    user.following.each do |followed|
      add_user followed
      create_follows_relationship followed: followed, follower: user
    end

    user.repositories.each do |repository|
      @neo4j_connector.add_repository_node repository.to_hash
      create_owns_relationship user: user, repository: repository
    end

    user.starred.each do |repository|
      @neo4j_connector.add_repository_node repository.to_hash
      create_stars_relationship user: user, repository: repository
    end
  end

  # Use it to add neighboured fetched users. User is added if it does not already exist.
  def add_user(user)
    if user_exists?(login: user.login)
      false
    else
      @neo4j_connector.add_user_node user.to_hash
      true
    end
  end

  def update_fetched_user(user,properties=nil)
    properties ||= user.to_hash
    update_user user, properties

    user.followers.each do |follower|
      add_user follower
      create_follows_relationship followed: user, follower: follower
    end

    user.following.each do |followed|
      add_user followed
      create_follows_relationship followed: followed, follower: user
    end

    user.repositories.each do |repository|
      @neo4j_connector.add_repository_node repository.to_hash
      create_owns_relationship user: user, repository: repository
    end

    user.starred.each do |repository|
      @neo4j_connector.add_repository_node repository.to_hash
      create_stars_relationship user: user, repository: repository
    end

  end

  def update_user(user, properties=nil)
    properties ||= user.to_hash
    @neo4j_connector.update_user_node_ghid user.id, properties
  end

  def delete_user(username)
    @neo4j_connector.delete_user_node_by login: username
  end

  # Repository methods.

  def fetch_repository_by_id(id)
    json = @neo4j_connector.get_repository_node_ghid id
    table = Neo4jResult.from_json json
    if table.empty?
      nil
    else
      repo_hash = table[0,0]
      contributor_table = Neo4jResult.from_json @neo4j_connector.get_ingoing_relationship repo_hash['id'], 'Contributes'
      stargazer_table = Neo4jResult.from_json @neo4j_connector.get_ingoing_relationship repo_hash['id'], 'Stars'
      repo_hash['contributors'] = contributor_table[:*,1].map { |u| Model::UserView.from_neo4j u}
      repo_hash['stargazers'] = stargazer_table[:*,1].map { |u| Model::UserView.from_neo4j u}

      Model::RepositoryView.from_neo4j repo_hash
    end
  end

  # Adds a fully fetched repository without checking if the repo is already present.
  def add_fetched_repository(repository)
    @neo4j_connector.add_repository_node repository.to_hash
    repository.contributors.each do |user|
      add_user user
      create_contributes_relationship user: user, repository: repository
    end

    repository.stargazers.each do |user|
      add_user user
      create_stars_relationship user: user, repository: repository
    end
  end

  # Use it to add neighboured fetched repositories. Repository is added if it does not already exist.
  def add_repository(repository)
    if repository_exists?(full_name: repository.full_name)
      false
    else
      @neo4j_connector.add_repository_node repository.to_hash
      true
    end
  end

  def update_fetched_repository(repository,properties=nil)
    properties ||= repository.to_hash
    update_repository repository, properties

    repository.contributors.each do |user|
      add_user user
      create_contributes_relationship user: user, repository: repository
    end

    repository.stargazers.each do |user|
      add_user user
      create_stars_relationship user: user, repository: repository
    end
  end

  def update_repository(repo, properties=nil)
    properties ||= repo.to_hash
    @neo4j_connector.update_repository_node_ghid repo.id, properties
  end

  # Relationship methods.

  def create_stars_relationship(params)
    user, repository, properties = params[:user], params[:repository], params[:properties]
    properties ||= {}
    if relationship_exists?(first_node: user, label: 'Stars', second_node: repository)
      false
    else
      @neo4j_connector.create_has_starred_relationship user.id, repository.id, properties
      true
    end
  end

  def create_owns_relationship(params)
    user, repository, properties = params[:user], params[:repository], params[:properties]
    properties ||= {}
    if relationship_exists?(first_node: user, label: 'Owns', second_node: repository)
      false
    else
      @neo4j_connector.create_owns_relationship user.id, repository.id, properties
      true
    end

  end

  def create_follows_relationship(params)
    followed, follower, properties = params[:followed], params[:follower], params[:properties]
    properties ||= {}
    if relationship_exists?(first_node: follower, label: 'Follows', second_node: followed)
      false
    else
      @neo4j_connector.create_follows_relationship follower.id, followed.id, properties
      true
    end
  end

  def create_contributes_relationship(params)
    user, repository, properties = params[:user], params[:repository], params[:properties]
    properties ||= {}
    properties = properties.merge(contributions: user.contributions) if !user.contributions.nil?

    if relationship_exists?(first_node: user, label: 'Contributes', second_node: repository)
      false
    else
      @neo4j_connector.create_contributes_relationship user.id, repository.id, properties
      true
    end
  end

end