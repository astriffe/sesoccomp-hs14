module Neo4jResult

  # factory method
  # parses the returned json result by the Neo4jConnector and returns a table object
  def self.from_json(json)
    a_hash = JSON.parse json
    Table.new a_hash
  end

  # class representing a row in a table
  class Row
    include Enumerable

    # represents a list as row
    def initialize(list)
      @list = list
    end

    def size
      @list.size
    end

    alias_method :length, :size

    def empty?
      size == 0
    end


    # ====Example
    # row[i] => returns the i-th column
    #
    def [](i)
      @list[i]
    end

    def each
      @list.each do |item|
          yield item
      end
    end

  end

  # class representing a table
  class Table
    include Enumerable
    attr_accessor :rows, :columns

    #
    # parses the passed json string returned by the neo4j restful api and represents the result as a 'table'
    #
    def initialize(params)
      result = params['results'].first
      @rows =  result['data'].map {|entry| Row.new entry['row']}
      @columns = result['columns']
    end

    # Provides array and matlab like access on this table object.
    # By means of the :* operator a column in the table can be selected.
    #
    # ====Examples
    # result[i] => returns the i-th row
    # result[:*,j] => returns all rows by considering only the j-th column
    # result[i,j] => returns the hash placed on the i-th row and j-th column
    # result[i,'column_name'] => returns the hash placed on the i-th row and column 'column_name'
    # result['column_name'] => returns all rows by considering only the column 'column_name'
    # result[:*,'column_name'] => returns all rows by considering only the column 'column_name'
    #
    def [](*args)
      index_i, index_j = *args
      case index_i
        when :*
          pick_column(index_j)
        when Fixnum
          if index_j.nil?
            @rows[index_i]
          elsif index_j.is_a? String
            index_j_num = columns.find_index { |column_name| column_name == index_j }
            row = @rows[index_i]
            row[index_j_num]
          else
            row = @rows[index_i]
            row[index_j]
          end
        when String
          pick_column(index_i)
        else
          nil
      end
    end

    def each
      @rows.each do |row|
       yield row
      end
    end

    def size
      @rows.size
    end

    def empty?
      size == 0
    end

    def pick_column(column_name)
      column_index = column_name.is_a?(Fixnum) ? column_name :  columns.find_index {|column| column == column_name}
      self.map {|row| row[column_index] }
    end

    alias_method :length, :size

  end

end