require 'octokit'

module GithubProxy

  CLIENT = Octokit::Client.new(
      :client_id => "c35f97b23fcf97b8f881",
      :client_secret => "15741b97a65c3025ac0b4890cace58118644b5e4")


  # Delegate all other calls to the octokit github client.
  def self.method_missing(method_name, *arguments, &block)
    CLIENT.send method_name, *arguments,&block
  end


  # Fetches a user from github by its login name.
  # Corresponds to a user click fetch.
  # This includes fetching the user repositories, starred repositories and a list of users (following)
  # that are followed by the given user.
  #
  # ====Examples
  # fetch_user 'wanze'
  # fetch_user 'lexruee'
  #
  def self.fetch_user(username)
    begin
      user = CLIENT.user username
    rescue
      raise 'Could not fetch user!'
    end

    # the following tasks are executed in a separate thread, so the variables like
    # repos, followers etc. are all empty until the join stmt of all tasks.
    tasks = []

    # get user repos
    repos = []
    tasks << Thread.new do
      repos = CLIENT.get user[:repos_url], per_page: 100
      repos = repos.map { |repo| Model::RepositoryView.from_github repo }
    end

    # get followers
    followers = []
    tasks << Thread.new do
      followers = CLIENT.get user[:followers_url], per_page: 100
      followers = followers.map {|follower| Model::UserView.from_github follower}
    end

    # get following
    following = []

    tasks << Thread.new do
      following = CLIENT.get user[:following_url].gsub(/{\/other_user}/,''), per_page: 100
      following = following.map { |follower| Model::UserView.from_github follower }
    end

    # get starred repos
    starred = []

    tasks << Thread.new do
      starred = CLIENT.get user[:starred_url].gsub(/{\/owner}{\/repo}/,''), per_page: 100
      starred = starred.map { |repo| Model::RepositoryView.from_github repo }
    end

    user_hash = user.to_hash

    # !!!!! join all tasks together!!!!!
    tasks.map {|task| task.join }

    # create a user object
    user_properties = user_hash.merge({ repositories: repos,
                                        followers: followers,
                                        following: following,
                                        starred: starred,
                                        full_fetch: true
                                      })

    Model::UserView.from_github user_properties
  end

  # Fetches a repository from github.
  # Corresponds to a repository click fetch.
  # This includes fetching the contributors of this repository.
  #
  # ====Examples
  # fetch_repository 'ruby/ruby'
  # fetch_repository 'rails/rails'
  #
  def self.fetch_repository(repository_full_name)
    begin
      # valid repo names are: {user}/{repo}  e.g.: ruby/ruby
      if (repository_full_name =~ /[a-zA-Z0-9_]+\/[a-zA-Z0-9_]+/) == nil
        raise 'Invalid repository name.'
      end
      repo = CLIENT.repository repository_full_name
    rescue
      raise 'Could not fetch repository!'
    end


    # the following tasks are executed in a separate thread, so the variables like
    # languages, contributors etc. are all empty until the join stmt of all tasks.
    tasks = []

    languages = {}
    # get languages stats (size in bytes)
    tasks << Thread.new do
      languages = CLIENT.languages repo[:full_name]
    end

    # get stargazers
    stargazers = []
    tasks << Thread.new do
      stargazers = CLIENT.stargazers repo[:full_name], per_page: 100
      stargazers = stargazers.map { |user| Model::UserView.from_github user }
    end

    # get contributors
    contributors = []
    tasks << Thread.new do
      contributors = CLIENT.get repo[:contributors_url], per_page: 100
      contributors = contributors.map { |user| Model::UserView.from_github user }
    end

    # !!!!! join all tasks together!!!!!
    tasks.map { |task| task.join }

    repo_hash = repo.to_hash
    repo_properties = repo_hash.merge({ contributors: contributors,
                                        stargazers: stargazers,
                                        full_fetch: true
                                      })


    lang_count = languages.to_hash.size
    lang_sizes = languages.to_hash.map {|k,v| v }
    lang_names = languages.to_hash.map {|k,v| k.to_s}


    repo_properties = repo_properties.merge(language_names: lang_names,
                                            language_sizes: lang_sizes,
                                            languages_count: lang_count)

    repo_properties =  repo_properties.merge(languages)

    Model::RepositoryView.from_github repo_properties
  end

end