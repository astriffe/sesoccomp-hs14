require 'net/http'
require 'uri'
require 'json'

$host = 'localhost'
$port = '7474'
$uri = URI.parse('http://localhost:7474/db/data/transaction/commit')
$transaction_path = '/db/data/transaction'

class Neo4jConnector

  #----------------------------------------------------------------------
  #	0.	For testing and development purposes
  #----------------------------------------------------------------------

  def query(query_string)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => query_string
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response= http.request(request)
    return response.body
  end

  # Removes all nodes and relationships in the database.
  def clear_database
    query 'MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r'
  end


  #----------------------------------------------------------------------
  #	1.	Node Management
  #----------------------------------------------------------------------

  #----------------------------------------------------------------------
  #	1.1		Private Generic Methods
  #----------------------------------------------------------------------
  private
  # @param [String] label - The label that is to be assigned to the node.
  # @param [Hash] properties - all properties of the node as a dictionary.
  def add_node(label, properties)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "CREATE (n:#{label} {props} ) RETURN n;",
            'parameters' => {
                'props' => properties,
                'lbl' => label
            }
        ]

    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json'})
    request.body = payload


    response= http.request(request)

    # For debug Purposes only. Todo FTO
    puts "Response #{response.code} #{response.message}: #{response.body}"

    response

  end

  # Gets a node with a specific type and a specific GitHub ID #
  def get_node_ghid(type, github_id)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n:#{type}) WHERE  n.id=#{github_id} RETURN n;"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response= http.request(request)
    return response.body
  end

  # Deletes a node using the specified label :label and property :property and its corresponding value :value.
  # ====Example
  # delete_node label: 'User', property: 'login', value: 'lexruee'
  #
  def delete_node(params)
    label, property, value = params[:label], params[:property], params[:value]
    value = "'#{value}'" if value.is_a? String # put a string value in single quotes
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n:#{label}) WHERE  n.#{property}=#{value} OPTIONAL MATCH (n)-[r]->() DELETE n,r;"
        # see: http://neo4j.com/docs/stable/query-delete.html#delete-delete-all-nodes-and-relationships
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response= http.request(request)
    return response.body
  end

  # Deletes a node with a specific type and a specific GitHub ID #
  def delete_node_ghid(type, github_id)
    delete_node label: type, property: :id, value: github_id
  end

  # Updates all the properties of a node with the given set of new properties.
  # Node matching is performed based on the Github ID.
  # NB: The node's properties will be replaced by the new set, i.e., properties
  #	that do not occur in the parameter 'properties' will not be present in the
  #	node's attribute set any longer.
  #
  # @param [String] - The node type, in our case either 'Repository' or 'User'
  # @param [Int] - The GitHub ID of the node that is to be modified.
  # @param [Hash] - The new property set.
  def update_node_properties_ghid(type, github_id, properties)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n:#{type}) WHERE  n.id=#{github_id} SET n = { props };",
            'parameters' => {
                'props' => properties,
                'lbl' => type
            }
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response= http.request(request)
    puts "Response #{response.code} #{response.message}: #{response.body}"
    return response.body
  end


  #----------------------------------------------------------------------
  #	1.2		Adding Nodes
  #----------------------------------------------------------------------
  public
  def add_user_node(properties)
    add_node('User', properties)
  end

  def add_repository_node(properties)
    add_node('Repository', properties)
  end

  #----------------------------------------------------------------------
  #	1.3		Getting Nodes
  #----------------------------------------------------------------------
  def get_all_nodes()
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n) RETURN n"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload


    response= http.request(request)

    # For debug Purposes only. Todo FTO
    #puts  "Response #{response.code} #{response.message}: #{response.body}"

    response.body

  end

  # Lookup user node according to its Github ID.  
  def get_user_node_ghid(github_user_id)
    get_node_ghid('User', github_user_id)
  end

  # Lookup repository according to its Github ID.
  def get_repository_node_ghid(github_repo_id)
    get_node_ghid('Repository', github_repo_id)
  end

  # Checks if a node of type :type with property :property and value :value exists.
  #
  # @param [Hash] - A hash with properties: :type, :property, :value
  def node_exists?(params)
    type, property, value = params[:type], params[:property], params[:value]
    http = Net::HTTP.new($uri.host, $uri.port)

    value = value.is_a?(String) ? "'#{value}'" : value

    payload = {
        'statements' => [
            'statement' => "MATCH (n:#{type}) WHERE  n.#{property}=#{value} RETURN n"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response= http.request(request)
    return response.body
  end

  # Lookup user node based on login name, i.e., attribute called 'login'.
  def exist_user_node(login)
    node_exists? type: 'User', property: 'login', value: login
  end

  # Lookup repository based on its full_name, i.e., attribute called 'full_name'.
  def exist_repository_node(name)
    node_exists? type: 'Repository', property: 'full_name', value: name
  end

  # Lookup user node based on github id, i.e., attribute called 'id'.
  def exist_user_node_ghid(ghid)
    node_exists? type: 'User', property: 'id', value: ghid
  end

  # Lookup user node based on github id, i.e., attribute called 'id'.
  def exist_repository_node_ghid(ghid)
    node_exists? type: 'Repository', property: 'id', value: ghid
  end

  #----------------------------------------------------------------------
  #	1.4		Modifying Nodes
  #----------------------------------------------------------------------
  def update_user_node_ghid(github_user_id, properties)
    update_node_properties_ghid('User', github_user_id, properties)
  end

  def update_repository_node_ghid(github_repo_id, properties)
    update_node_properties_ghid('Repository', github_repo_id, properties)
  end


  #----------------------------------------------------------------------
  #	1.5		Deleting Nodes
  #----------------------------------------------------------------------

  # ====Examples
  # delete_user_node_by login: 'lexruee'
  # delete_user_node_by id: 3432
  #
  def delete_user_node_by(a_hash)
    key, value = a_hash.keys.first, a_hash.values.first
    delete_node label: 'User', property: key, value: value
  end

  def delete_user_node_ghid(github_user_id)
    delete_node_ghid('User', github_user_id)
  end

  def delete_repository_node_ghid(github_repo_id)
    delete_node_ghid('Repository', github_repo_id)
  end

  #----------------------------------------------------------------------
  #	2.	Relationship Management
  #----------------------------------------------------------------------
  private
  # @param [int] n1_id - github ID of Node 1, which is of Type n1_label (outgoing)
  # @param [int] n2_id - github ID of Node 2, which is of Type n2_label (ingoing)
  # @param [String] n1_label - label of Node 1, which is of Type n1_label (outgoing)
  # @param [String] n2_label - label ID of Node 2, which is of Type n2_label (ingoing)
  # @param [String] label - Relationship type.

  # @param [Hash] properties - the properties to add to the relationship
  def create_relationship(n1_id, n2_id, n1_label, n2_label, label, properties)
    # TODO CreateIssueIfNecessary: Currently, lookup is performed after neo4j node id. GitHub ID as parameter was also possible.
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n1:#{n1_label}), (n2:#{n2_label}) WHERE n1.id= #{n1_id} AND n2.id=#{n2_id}
                            CREATE (n1)-[:#{label} {props}]->(n2);",
            'parameters' => {
                'props' => properties
            }
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response= http.request(request)
    puts "Response #{response.code} #{response.message}: #{response.body}"
    response.body
  end

  public
  def create_owns_relationship(user_id, repo_id, properties)
    create_relationship(user_id, repo_id, 'User', 'Repository', 'Owns', properties)
  end

  def create_has_starred_relationship(user_id, repo_id, properties)
    create_relationship(user_id, repo_id, 'User', 'Repository', 'Stars', properties)
  end

  def create_contributes_relationship(user_id, repo_id, properties)
    create_relationship(user_id, repo_id, 'User', 'Repository', 'Contributes', properties)
  end

  def create_follows_relationship(user1_id, user2_id, properties)
    create_relationship(user1_id, user2_id, 'User', 'User', 'Follows', properties)
  end


  def get_outgoing_relationship(node_id, label)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n1) - [r:#{label}] -> (peer) WHERE n1.id=#{node_id} RETURN r, peer;"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload


    response= http.request(request)
    response.body
  end

  def get_ingoing_relationship(node_id, label)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (peer) - [r:#{label}] -> (n2) WHERE n2.id=#{node_id} RETURN r, peer;"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload


    response= http.request(request)
    response.body
  end


  def get_relationships_between_nodes(node1_id, node2_id, label=nil)
    http = Net::HTTP.new($uri.host, $uri.port)

    label = label.nil? ? '' : ":#{label}"

    payload = {
        'statements' => [
            'statement' => "MATCH (n1) - [r#{label}] -> (n2) WHERE n1.id=#{node1_id} AND n2.id=#{node2_id} RETURN n1, r, n2;"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload


    response= http.request(request)
    response.body
  end

  def get_relationship(label)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n1) - [r:#{label}] -> (n2) RETURN n1, r, n2;"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload


    response= http.request(request)
    response.body
  end


  def find_user_shortest_path(user_ghid_1, user_ghid_2, max_distance)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (m:User { id:#{user_ghid_1}}),(n:User { id:#{user_ghid_2}}),
            p = shortestPath((m)-[*..#{max_distance}]-(n)) RETURN p"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload


    response= http.request(request)
    response.body

  end


  def find_shortest_path(q1, q2, max_distance=5)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (m { q: '#{q1}'}),(n { q: '#{q2}'}),
            p = shortestPath((m)-[*..#{max_distance}]-(n))
            RETURN nodes(p) as nodes,extract(n in nodes(p)|head(labels(n))) as node_labels,extract( r in relationships(p)|type(r)) as rel_labels;"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response = http.request(request)

    response.body
  end


  def find_paths(q1, q2, limit=25, max_length=3)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' =>
                "MATCH (m { q: '#{q1}'}),(n { q: '#{q2}'}),
            p = (m)-[*1..#{max_length}]-(n)
            RETURN nodes(p) as nodes,extract(n in nodes(p)|head(labels(n))) as node_labels,extract( r in relationships(p)|type(r)) as rel_labels LIMIT #{limit};"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response = http.request(request)

    response.body


  end


  #
  # ====Examples
  # find_repository_with [[ 'languages_count', '<', 5], 'or', [ 'language', '=', 'Ruby']]
  # find_repository_with [[ 'languages_count', '>', 1], 'and', [ 'languages_count', '<', 5]]
  # find_repository_with [[ 'language', '=', 'Ruby']]
  #
  def find_repository_with(conditions, limit=25)
    condition_string = parse_conditions(conditions)

    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n:`Repository`) #{condition_string} RETURN n LIMIT #{limit}"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response = http.request(request)

    response.body
  end


  def find_random_repository(conditions)
    condition_string = parse_conditions(conditions)
    http = Net::HTTP.new($uri.host, $uri.port)

    payload = {
        'statements' => [
            'statement' => "MATCH (n:`Repository`) #{condition_string} RETURN n, rand() as r ORDER BY r LIMIT 1"
        ]
    }.to_json

    request = Net::HTTP::Post.new($uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
    request.body = payload

    response = http.request(request)

    response.body
  end


  def parse_conditions(conditions)
    ops = ['<', '<=', '>', '>=', '=', 'in']
    connectives = ['and', 'or']

    condition_strings = conditions.map do |condition|
      case condition
        when String
          raise 'error' unless connectives.include?(condition)
          " #{condition} "
        else
          property, op, value = condition
          raise 'error' unless ops.include?(op)
          if op == 'in'
            property = "'#{property}'" if property.is_a?(String)
            "#{property} #{op} n.#{value}"
          else
            value = "'#{value}'" if value.is_a?(String)
            "n.#{property} #{op} #{value}"
          end
      end
    end

    if condition_strings.size > 0
      'WHERE ' + condition_strings.join(' ')
    else
      ''
    end
  end


end

