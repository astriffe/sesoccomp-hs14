
$(document).ready(function(){
    $('.ui.sidebar').sidebar();
    $('.ui.checkbox').checkbox();

    var checkboxes = [];

    $('.ui.checkbox').on('change', function() {
        var $checkbox = $(this);

    });


    $('#git-buddy-zones .button').popup( {
        on: 'hover',
        position : 'bottom left'
    });

    $('#modal-start').modal('show');

});


var width = $(window).width();
var height = $(window).height();

var radius = 15;

var data = {
	"nodes" : [
		{ "type" : "user", "label" : "wanze", 'x' : width/2 - radius, 'y' : height/2 - radius },
	],
	'links' : []
}

var force = d3.layout.force()
    .friction(0.8)
    .linkDistance(200)
    .gravity(0.0)
    .charge(-250)
    .chargeDistance(250)
    .size([width, height])
	.nodes(data.nodes)
	.links(data.links)
	.on('tick', tick);
	
var svg = d3.select('#git-buddy-explore')
    .attr("width", width)
    .attr("height", height)
    .attr('viewBox', '0 0 ' + width + ' ' + height);


var node = svg.selectAll('.node');
var link = svg.append('svg:g').selectAll('.link');

function tick() {
	//node.attr("cx", function(d) { return d.x; })
    	//.attr("cy", function(d) { return d.y; });
    node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
    link.attr("x1", function(d) { return d.source.x; })
      	.attr("y1", function(d) { return d.source.y; })
	  	.attr("x2", function(d) { return d.target.x; })
	  	.attr("y2", function(d) { return d.target.y; });
}

// Visited node, don't reload data on click
var visited = [];

// Refresh graph
var refreshGraph = function(node_clicked, new_data) {

	link = link.data(force.links());

    var active_links = [];

    // Update link data
    link.attr('class', function(l) {
        // Set class to inactive if new data was loaded, otherwise if node was clicked and data already loaded, flag as active
        var _class = (new_data == false && l.source == node_clicked) ? 'active' : 'inactive';
        if (_class == 'active') {
            active_links.push(l);
        }
        return 'link ' + _class + ' ' + l.type;
    }).attr('marker-end', function(l) {
        var $link = d3.select(this);
        return ($link.classed('inactive')) ? 'url(#inactive)' : 'url(#' + l.type + ')';
    });

    // New link data
	link.enter()
		.insert('line', '.node')
		// Only the links going to the clicked node are flagged as active
        .attr('class', function(l) {
            var _class = (l.source == node_clicked) ? 'active' : 'inactive';
            return 'link ' + _class + ' ' + l.type;
        })
        .attr('marker-end', function(l) {
            return 'url(#' + l.type + ')';
        });

	// Removed link data
    link.exit().remove();

	node = node.data(force.nodes());

    // Update node data
    node.attr('class', function(d) {
        var _class = 'inactive';
        for (var i=0; i<active_links.length; i++) {
            if (d == active_links[i].target) {
                _class = 'active';
                break;
            }
        }
        return 'node ' + _class + ' ' + d.type;
    });

    // New node data
    var g = node.enter()
        .append('g')
	    .attr("class", function(d) { return 'node active ' + d.type; })
		.on('click', onNodeClick)
		.on('dblclick', onNodeDblClick)
        .on('mouseover', onNodeHover)
        .on('mouseleave', onNodeMouseOut)
        .call(force.drag);
    g.append("circle").attr("r", radius);
    g.append('text')
        .attr('class', 'label')
        .attr('x', 0)
        .attr('dy', '2em')
        .attr('text-anchor', 'middle')
        .text(function(d) { return d.label });

    // Removed node data
    node.exit().remove();

    force.start();
}

// On node click
var onNodeClick = function(node) {
	if (d3.event.defaultPrevented) return; // ignore drag
	//console.log('clicked node ' + node);
	node.fixed = true;
    var new_data = false;
    if (visited.indexOf(node.index) == -1) {
        new_data = true;
        getNewData(node);
        $('#loader').addClass('active');
        setTimeout(function() {
            $('#loader').removeClass('active');
            // Display infos about node in right overlay
            var $info = $('#git-buddy-info');
            $info.find('h2').html('<i class="user icon"></i> ' + node.label);
            if ( ! $info.hasClass('visible')) {
                console.log('not visible');
                $info.sidebar({ overlay: true}).sidebar('show');
            }
        }, 250);
    }
	refreshGraph(node, new_data);
	visited.push(node.index);

	// Hightlight the current node selected
    var $node = d3.select(this);
    $node.classed('selected active', true);
    $node.classed('inactive', false);
    $node.select('circle').attr('r', radius * 1.1);
}

// On node doubleclick
var onNodeDblClick = function(node) {
	var box_coordinates = svg.attr('viewBox').split(' ');
	var x = node.x - (width/2 - radius);
	var y = node.y - (height/2 - radius);
	svg.attr('viewBox', x + ' ' + y + ' ' + box_coordinates[2] + ' ' + box_coordinates[3]);
}

var onNodeHover = function(node) {
    var $node = d3.select(this);
    $node.classed($node.attr('class') + ' hover', true);
}

var onNodeMouseOut = function(node) {
    var $node = d3.select(this);
    $node.classed('hover', false);
}


var total = 0;
// Generate new random data
var getNewData = function(node) {
	// Generate some sample nodes
	var n = getRandomInt(total, total+20);
	for (var i=total; i<n; i++) {
		var node_type = (i % 2 == 0) ? 'user' : 'repository';
        var new_node = {"type" : node_type, "label" : node_type + ' ' + i};
		data.nodes.push(new_node);
		// Add link from clicked node
		var link_type = ''
        if (node_type == 'user') {
            link_type = 'follows';
        } else {
            link_type = (getRandomInt(0,10) > 7) ? 'starred' : 'owns';
        }
        data.links.push({ 'source' : node, 'target' : new_node, 'type' : link_type });
		// Generate some sample links to other nodes from the new node
		//var n_links = getRandomInt(0,1);
		//for (var j=0; j<n_links; j++) {
		//	var node_index = getRandomInt(1, data.nodes.length-1);
		//	// Get type of node
         //   var node_type = data.nodes[node_index].type;
         //   var link_type = (node_type == 'user') ? 'follows' : 'starred';
         //   data.links.push({ 'source' : new_node, 'target' : data.nodes[node_index], 'type' : link_type});
		//}
// 		console.log(data);
	}
    total = n;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Start
refreshGraph();